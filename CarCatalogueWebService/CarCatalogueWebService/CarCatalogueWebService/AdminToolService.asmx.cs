﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CarCatalogueWebService
{
    /// <summary>
    /// Summary description for AdminToolService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AdminToolService : System.Web.Services.WebService
    {

        private static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string path = AppDomain.CurrentDomain.BaseDirectory;
        private static string dbPath = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + path + @"App_Data\WebServiceBD.mdf;Integrated Security = True";
        private SqlConnection Connection = new SqlConnection(connectionString: dbPath);
        private SqlDataAdapter daUsers;
        private DataSet dsUsers;

        [WebMethod]
        public DataSet PopulateUsers(bool isReported, bool isBanned)
        {
            dsUsers = new DataSet();
            Connection.Open();
            if (isReported && !isBanned) 
            { 
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE IsReported = @valIsReported", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@valIsReported", 1);
                daUsers.Fill(dsUsers, "Useri");
                Connection.Close();
                return dsUsers;
            }
            if (isReported && isBanned)
            {
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE IsReported = @valIsReported AND IsBanned = @valIsBanned", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@valIsReported", 1);
                daUsers.SelectCommand.Parameters.AddWithValue("@valIsBanned", 1);
                daUsers.Fill(dsUsers, "Useri");
                Connection.Close();
                return dsUsers;
            }
            if(isBanned && !isReported)
            {
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE IsBanned = @valIsBanned", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@valIsBanned", 1);
                daUsers.Fill(dsUsers, "Useri");
                Connection.Close();
                return dsUsers;
            }
            if(!isBanned && !isReported)
            {
                daUsers = new SqlDataAdapter("SELECT * FROM Useri", Connection);
                daUsers.Fill(dsUsers, "Useri");
                Connection.Close();
                return dsUsers;
            }
            Connection.Close();
            return dsUsers;
        }

        [WebMethod]
        public DataSet SearchUsers(bool isReported, bool isBanned, int id)
        {
            dsUsers = new DataSet();
            Connection.Open();
           
            if (isReported && !isBanned)
            {
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE IsReported = @valIsReported AND Id = @valId", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@valIsReported", 1);
                daUsers.SelectCommand.Parameters.AddWithValue("@valId", id);
                daUsers.Fill(dsUsers, "Useri");
                Connection.Close();
                return dsUsers;
            }
            if (isReported && isBanned)
            {
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE IsReported = @valIsReported AND IsBanned = @valIsBanned AND Id = @valId", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@valIsReported", 1);
                daUsers.SelectCommand.Parameters.AddWithValue("@valIsBanned", 1);
                daUsers.SelectCommand.Parameters.AddWithValue("@valId", id);
                daUsers.Fill(dsUsers, "Useri");
                Connection.Close();
                return dsUsers;
            }
            if (isBanned && !isReported)
            {
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE IsBanned = @valIsBanned AND Id = @valId", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@valIsBanned", 1);
                daUsers.SelectCommand.Parameters.AddWithValue("@valId", id);
                daUsers.Fill(dsUsers, "Useri");
                Connection.Close();
                return dsUsers;
            }
            if (!isBanned && !isReported)
            {
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE Id = @valId", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@valId", id);
                daUsers.Fill(dsUsers, "Useri");
                Connection.Close();
                return dsUsers;
            }
            Connection.Close();
            return dsUsers;
        }

        [WebMethod]
        public void banUser(int id)
        {
            Connection.Open();

            SqlCommand cmd = new SqlCommand("UPDATE Useri SET IsBanned = @valIsBanned WHERE Id = @valId", Connection);
            cmd.Parameters.AddWithValue("@valIsBanned", 1);
            cmd.Parameters.AddWithValue("@valId", id);
            cmd.ExecuteNonQuery();
            Connection.Close();
        }

        [WebMethod]
        public void unbanUser(int id)
        {
            Connection.Open();

            SqlCommand cmd = new SqlCommand("UPDATE Useri SET IsBanned = @valIsBanned WHERE Id = @valId", Connection);
            cmd.Parameters.AddWithValue("@valIsBanned", 0);
            cmd.Parameters.AddWithValue("@valId", id);
            cmd.ExecuteNonQuery();
            Connection.Close();
        }

        [WebMethod]
        public void unreportUser(int id)
        {
            Connection.Open();

            SqlCommand cmd = new SqlCommand("UPDATE Useri SET IsReported = @valIsReported WHERE Id = @valId", Connection);
            cmd.Parameters.AddWithValue("@valIsReported", 0);
            cmd.Parameters.AddWithValue("@valId", id);
            cmd.ExecuteNonQuery();
            Connection.Close();
        }

        [WebMethod]
        public void deleteUser(int id)
        {
            Connection.Open();

            SqlCommand cmd = new SqlCommand("DELETE FROM Anunturi WHERE Id = @valId", Connection);
            cmd.Parameters.AddWithValue("@valId", id);
            cmd.ExecuteNonQuery();
            Connection.Close();

            Connection.Open();
            cmd = new SqlCommand("DELETE FROM Useri WHERE Id = @valId", Connection);
            cmd.Parameters.AddWithValue("@valId", id);
            cmd.ExecuteNonQuery();
            Connection.Close();
        }

    }
}
