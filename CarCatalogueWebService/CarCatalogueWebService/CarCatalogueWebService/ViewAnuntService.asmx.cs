﻿using CarCatalogueWebService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CarCatalogueWebService
{
    /// <summary>
    /// Summary description for ViewAnuntService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ViewAnuntService : System.Web.Services.WebService
    {
        private static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string path = AppDomain.CurrentDomain.BaseDirectory;
        private static string dbPath = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + path + @"App_Data\WebServiceBD.mdf;Integrated Security = True";
        private SqlConnection Connection = new SqlConnection(connectionString: dbPath);
        private SqlDataAdapter daAnunturi;
        private DataSet dsAnunturi;

        [WebMethod]
        public AnnounceHandler getAnounceData(int codAnunt)
        {
            AnnounceHandler anunt;
            dsAnunturi = new DataSet();
            Connection.Open();

            daAnunturi = new SqlDataAdapter("SELECT * FROM Anunturi WHERE CodAnunt = @valCodAnunt", Connection);
            daAnunturi.SelectCommand.Parameters.AddWithValue("@valCodAnunt", codAnunt);
            daAnunturi.Fill(dsAnunturi, "Anunt");

            Connection.Close();

            DataRow dr = dsAnunturi.Tables["Anunt"].Rows[0];

                int id = int.Parse(dr["Id"].ToString());
                string caroserie = dr["Caroserie"].ToString();
                string marca = dr["Marca"].ToString();
                string model = dr["Model"].ToString();
                string varianta = dr["Varianta"].ToString();
                int pret = int.Parse(dr["PretDeVanzare"].ToString());
                int pretlic = int.Parse(dr["PretDeStartLicitatie"].ToString());
                int an = int.Parse(dr["AnPrimaInmatriculare"].ToString());
                int km = int.Parse(dr["Kilometri"].ToString());
                int cp = int.Parse(dr["Putere"].ToString());
                int kw = int.Parse(dr["PutereKw"].ToString());
                string combustibil = dr["Combustibil"].ToString();
                string cutie = dr["CutieDeViteze"].ToString();
                int cc = int.Parse(dr["CapacitateCilindrica"].ToString());
                string culoare = dr["Culoare"].ToString();
                string data = dr["DataAdaugareAnunt"].ToString();
                string locatie = dr["Locatie"].ToString();
                string persoana = dr["FirmaPersoanaFizica"].ToString();
                string descriere = dr["Descriere"].ToString();
                byte[] imagAnung = (byte[])dr["ImagineAnunt"];
                byte[] im1 = (byte[])dr["Imagine1"];
                byte[] im2 = (byte[])dr["Imagine2"];
                byte[] im3 = (byte[])dr["Imagine3"];
                int lastlic = int.Parse(dr["LastLicitantID"].ToString());
                

            Connection.Open();
            daAnunturi = new SqlDataAdapter("SELECT * FROM Useri WHERE Id = @valId", Connection);
            daAnunturi.SelectCommand.Parameters.AddWithValue("@valId", id);
            daAnunturi.Fill(dsAnunturi, "User");
            Connection.Close();

            DataRow drn = dsAnunturi.Tables["User"].Rows[0];

            byte[] imUser = (byte[])drn["ImaginePortret"];
            string telefon = drn["Telefon"].ToString();
            string nume = drn["Nume"].ToString();
            string prenume = drn["Prenume"].ToString();

            anunt = new AnnounceHandler(id, codAnunt, caroserie, marca, model, varianta, pret, pretlic, an, km, cp, kw, combustibil, cutie, cc, culoare, data, locatie, persoana, descriere, imagAnung, im1, im2, im3, imUser, nume, prenume, telefon, lastlic);

            return anunt;
        }

        [WebMethod]
        public void updateLic(int codAnunt, int lic, int userLic)
        {
            Connection.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Anunturi SET PretDeStartLicitatie = @valLic, LastLicitantID = @valUser WHERE CodAnunt = @valCod", Connection);
            cmd.Parameters.AddWithValue("@valLic", lic);
            cmd.Parameters.AddWithValue("@valUser", userLic);
            cmd.Parameters.AddWithValue("@valCod", codAnunt);
            cmd.ExecuteNonQuery();
            Connection.Close();
        }

        [WebMethod]
        public void updateReport(int idUser, int CodAnuntRaportat)
        {
            Connection.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Useri SET IsReported = @valIsReported, CodAnuntRaportat = @valCod WHERE Id = @valId", Connection);
            cmd.Parameters.AddWithValue("@valCod", CodAnuntRaportat);
            cmd.Parameters.Add("@valIsReported", 1);
            cmd.Parameters.Add("@valId", idUser);
            cmd.ExecuteNonQuery();
            Connection.Close();
        }

        [WebMethod]
        public void deleteAnunt(int CodAnunt)
        {
            Connection.Open();
            SqlCommand cmd = new SqlCommand("DELETE FROM Anunturi WHERE CodAnunt = @valCodAnunt", Connection);
            cmd.Parameters.AddWithValue("@valCodAnunt", CodAnunt);
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
    }
}
