﻿using CarCatalogueWebService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

namespace CarCatalogueWebService
{
    /// <summary>
    /// Summary description for LoginService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class LoginService : System.Web.Services.WebService
    {

        private static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string path = AppDomain.CurrentDomain.BaseDirectory;
        private static string dbPath = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename="+path+@"App_Data\WebServiceBD.mdf;Integrated Security = True";
        private SqlConnection Connection = new SqlConnection(connectionString: dbPath);
        private SqlDataAdapter daUsers, daAdmins;
        private DataSet dsUsers, dsAdmins;




        private DataSet findEmails(string email, bool isAdmin)
        {
            Console.WriteLine(dbPath);
            DataSet emails = new DataSet();
        

            if (isAdmin)
            {
                Connection.Open();
                daAdmins = new SqlDataAdapter("SELECT * FROM Administratori WHERE Email LIKE @emailval", Connection);
                daAdmins.SelectCommand.Parameters.AddWithValue("@emailval", email);
                daAdmins.Fill(emails, "Admini");
                Connection.Close();
            }

            else
            {
                Connection.Open();
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE Email LIKE @emailval", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@emailval", email);
                daUsers.Fill(emails, "Useri");
                Connection.Close();
            }
            
            return emails;
        }





        [WebMethod]
        public bool EmailIsFoundInDataBase(string email, bool isAdmin)
        {
            bool isFound = false;

            if (isAdmin)
            {
                dsAdmins = findEmails(email, isAdmin);
                if (dsAdmins.Tables["Admini"].Rows.Count != 0) isFound = true;
            }
            else
            {
                dsUsers = findEmails(email, isAdmin);
                if (dsUsers.Tables["Useri"].Rows.Count != 0) isFound = true;
            }
            return isFound;
        }





        [WebMethod]
        public bool CheckPassword(string email, string parola, bool isAdmin)
        {
            bool isOk = false;
            if (isAdmin)
            {
                dsAdmins = findEmails(email, isAdmin);
                foreach (DataRow dr in dsAdmins.Tables["Admini"].Rows)
                {
                    if (dr.ItemArray.GetValue(4).ToString().Trim().Equals(parola))
                    {
                        isOk = true;
                        break;
                    }
                }
            }
            else
            {
                dsUsers = findEmails(email, isAdmin);
                foreach (DataRow dr in dsUsers.Tables["Useri"].Rows)
                {
                    if (dr.ItemArray.GetValue(4).ToString().Trim().Equals(parola))
                    {
                        isOk = true;
                        break;
                    }
                }
            }
            return isOk;
        }

        [WebMethod]
        public UserHandler ReturnUserFromDataBase(string email)
        {
            DataSet emails = new DataSet();
            UserHandler handledUser;


            Connection.Open();
            daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE Email LIKE @emailval", Connection);
            daUsers.SelectCommand.Parameters.AddWithValue("@emailval", email);
            daUsers.Fill(emails, "Useri");
            Connection.Close();

            DataRow dr = emails.Tables["Useri"].Rows[0];

            int id = Int32.Parse(dr["Id"].ToString());
            string nume = dr["Nume"].ToString();
            string prenume = dr["Prenume"].ToString();
            string email2 = dr["Email"].ToString();
            string telefon = dr["Telefon"].ToString();
            string adresa = dr["Adresa"].ToString();
            int nrAnunturi = Int32.Parse(dr["NrAnunturi"].ToString());
            
            int isBanned;
            if (dr["IsBanned"].ToString().Equals("0")) isBanned = 0;
            else isBanned = 1;
            int isReported;
            if (dr["IsReported"].ToString().Equals("0")) isReported = 0;
            else isReported = 1;
            
            int codAnuntRaportat = Int32.Parse(dr["CodAnuntRaportat"].ToString());
            string observatii = dr["Observatii"].ToString();
            byte[] imageArray = (byte[])dr["ImaginePortret"];

            handledUser = new UserHandler(id, nrAnunturi, codAnuntRaportat, nume, prenume, email2, adresa, telefon, observatii, isBanned, isReported, imageArray);


            return handledUser;
        }

        [WebMethod]
        public AdminHandler ReturnAdminFromDataBase(string email)
        {
            DataSet emails = new DataSet();
            AdminHandler handledAdmin;


            Connection.Open();
            daUsers = new SqlDataAdapter("SELECT * FROM Administratori WHERE Email LIKE @emailval", Connection);
            daUsers.SelectCommand.Parameters.AddWithValue("@emailval", email);
            daUsers.Fill(emails, "Admini");
            Connection.Close();

            DataRow dr = emails.Tables["Admini"].Rows[0];

            int id = Int32.Parse(dr["Id"].ToString());
            string nume = dr["Nume"].ToString();
            string prenume = dr["Prenume"].ToString();
            string email2 = dr["Email"].ToString();
            string contact = dr["Contact"].ToString();
            string observatii = dr["Observatii"].ToString();
            byte[] imageArray = (byte[])dr["ImaginePortret"];

            handledAdmin = new AdminHandler(id, nume, prenume, email2, contact, observatii, imageArray);
            return handledAdmin;
        }
    }
}
