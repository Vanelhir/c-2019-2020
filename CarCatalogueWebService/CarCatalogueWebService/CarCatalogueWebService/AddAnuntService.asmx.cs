﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CarCatalogueWebService
{
    /// <summary>
    /// Summary description for AddAnuntService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

               
    public class AddAnuntService : System.Web.Services.WebService
    {


        private static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string path = AppDomain.CurrentDomain.BaseDirectory;
        private static string dbPath = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + path + @"App_Data\WebServiceBD.mdf;Integrated Security = True";
        private SqlConnection Connection = new SqlConnection(connectionString: dbPath);
        private SqlDataAdapter daAnunturi;
        private DataSet dsAnunturi;

        [WebMethod]
        public void addAnuntLaDataBase(int id, string caroserie, string marca, string model, string varianta,
            int pret, int lic, int an, int km, int putere, int puterekw, string combustibil, string cutieviteze,
            int cc, string culoare, string locatie, string persoana, string descriere, byte[] ima, byte[] im1, byte[] im2, byte[] im3, int licId)
        {
            int code = getLastCode();
            Connection.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO Anunturi (Id, CodAnunt, Caroserie, Marca, Model, Varianta, PretDeVanzare, " +
                "PretDeStartLicitatie, AnPrimaInmatriculare, Kilometri, Putere, Puterekw, Combustibil, CutieDeViteze, CapacitateCilindrica, " +
                "Culoare, DataAdaugareAnunt, Locatie, FirmaPersoanaFizica, Descriere, ImagineAnunt, Imagine1, Imagine2, Imagine3, LastLicitantID) " +
                "VALUES (@valId, @valCodAnunt, @valCaroserie, @valMarca, @valModel, @valVarianta, @valPretDeVanzare, @valPretDeStartLicitatie, " +
                "@valAnPrimaInmatriculare, @valKilometri, @valPutere, @valPuterekw, @valCombustibil, @valCutieDeViteze, @valCapacitateCilindrica, " +
                "@valCuloare, @valDataAdaugareAnunt, @valLocatie, @valFirmaPersoanaFizica, @valDescriere, @valImagineAnunt, @valImagine1, @valImagine2, " +
                "@valImagine3, @valLastLicitantID)", Connection);

            DateTime thisDay = DateTime.Today;
            string thisDays = thisDay.ToString("d");

            cmd.Parameters.AddWithValue("@valId", id);
            cmd.Parameters.AddWithValue("@valCodAnunt", code);
            cmd.Parameters.AddWithValue("@valCaroserie", caroserie);
            cmd.Parameters.AddWithValue("@valMarca", marca);
            cmd.Parameters.AddWithValue("@valModel", model);
            cmd.Parameters.AddWithValue("@valVarianta", varianta);
            cmd.Parameters.AddWithValue("@valPretDeVanzare", pret);
            cmd.Parameters.AddWithValue("@valPretDeStartLicitatie", lic);
            cmd.Parameters.AddWithValue("@valAnPrimaInmatriculare", an);
            cmd.Parameters.AddWithValue("@valKilometri", km);
            cmd.Parameters.AddWithValue("@valPutere", putere);
            cmd.Parameters.AddWithValue("@valPuterekw", puterekw);
            cmd.Parameters.AddWithValue("@valCombustibil", combustibil);
            cmd.Parameters.AddWithValue("@valCutieDeViteze", cutieviteze);
            cmd.Parameters.AddWithValue("@valCapacitateCilindrica", cc);
            cmd.Parameters.AddWithValue("@valCuloare", culoare);
            cmd.Parameters.AddWithValue("@valDataAdaugareAnunt", thisDays);
            cmd.Parameters.AddWithValue("@valLocatie", locatie);
            cmd.Parameters.AddWithValue("@valFirmaPersoanaFizica", persoana);
            cmd.Parameters.AddWithValue("@valDescriere", descriere);
            cmd.Parameters.AddWithValue("@valImagineAnunt", ima);
            cmd.Parameters.AddWithValue("@valImagine1", im1);
            cmd.Parameters.AddWithValue("@valImagine2", im2);
            cmd.Parameters.AddWithValue("@valImagine3", im3);
            cmd.Parameters.AddWithValue("@valLastLicitantID", licId);
            cmd.ExecuteNonQuery();
            
            Connection.Close();


        }

        [WebMethod]
        public void updateAnuntInDataBase(int code, string caroserie, string marca, string model, string varianta,
    int pret, int lic, int an, int km, int putere, int puterekw, string combustibil, string cutieviteze,
    int cc, string culoare, string locatie, string persoana, string descriere, byte[] ima, byte[] im1, byte[] im2, byte[] im3)
        {
            Connection.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Anunturi SET Caroserie = @valCaroserie, Marca = @valMarca, Model = @valModel, Varianta = @valVarianta, " +
                "PretDeVanzare = @valPretDeVanzare, PretDeStartLicitatie = @valPretDeStartLicitatie, AnPrimaInmatriculare = @valAnPrimaInmatriculare, Kilometri = @valKilometri, " +
                "Putere = @valPutere, PutereKw = @valPuterekw, Combustibil = @valCombustibil, CutieDeViteze = @valCutieDeViteze, CapacitateCilindrica = @valCapacitateCilindrica, Culoare = @valCuloare, " +
                "Locatie = @valLocatie, FirmaPersoanaFizica = @valFirmaPersoanaFizica, Descriere = @valDescriere, ImagineAnunt = @valImagineAnunt, Imagine1 = @valImagine1, Imagine2 = @valImagine2, " +
                "Imagine3 = @valImagine3 WHERE CodAnunt = @valCodAnunt", Connection);


            cmd.Parameters.AddWithValue("@valCodAnunt", code);
            cmd.Parameters.AddWithValue("@valCaroserie", caroserie);
            cmd.Parameters.AddWithValue("@valMarca", marca);
            cmd.Parameters.AddWithValue("@valModel", model);
            cmd.Parameters.AddWithValue("@valVarianta", varianta);
            cmd.Parameters.AddWithValue("@valPretDeVanzare", pret);
            cmd.Parameters.AddWithValue("@valPretDeStartLicitatie", lic);
            cmd.Parameters.AddWithValue("@valAnPrimaInmatriculare", an);
            cmd.Parameters.AddWithValue("@valKilometri", km);
            cmd.Parameters.AddWithValue("@valPutere", putere);
            cmd.Parameters.AddWithValue("@valPuterekw", puterekw);
            cmd.Parameters.AddWithValue("@valCombustibil", combustibil);
            cmd.Parameters.AddWithValue("@valCutieDeViteze", cutieviteze);
            cmd.Parameters.AddWithValue("@valCapacitateCilindrica", cc);
            cmd.Parameters.AddWithValue("@valCuloare", culoare);
            cmd.Parameters.AddWithValue("@valLocatie", locatie);
            cmd.Parameters.AddWithValue("@valFirmaPersoanaFizica", persoana);
            cmd.Parameters.AddWithValue("@valDescriere", descriere);
            cmd.Parameters.AddWithValue("@valImagineAnunt", ima);
            cmd.Parameters.AddWithValue("@valImagine1", im1);
            cmd.Parameters.AddWithValue("@valImagine2", im2);
            cmd.Parameters.AddWithValue("@valImagine3", im3);
            cmd.ExecuteNonQuery();

            Connection.Close();


        }

        private int getLastCode()
        {
            int code = 0;

            Connection.Open();
            dsAnunturi = new DataSet();
            daAnunturi = new SqlDataAdapter("SELECT TOP 1 * FROM Anunturi ORDER BY CodAnunt DESC", Connection);
            daAnunturi.Fill(dsAnunturi, "Anunturi");
            foreach (DataRow dr in dsAnunturi.Tables["Anunturi"].Rows)
            {
                bool success = int.TryParse(dr.ItemArray.GetValue(1).ToString(), out code);
                if (success)
                {
                    code++;
                }
                else
                {
                    code = 0;
                }
            }
            Connection.Close();
            return code;
        }
    }
}
