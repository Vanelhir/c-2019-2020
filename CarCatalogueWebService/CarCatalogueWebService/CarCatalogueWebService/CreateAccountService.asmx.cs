﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CarCatalogueWebService
{
    /// <summary>
    /// Summary description for CreateAccountService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CreateAccountService : System.Web.Services.WebService
    {
        private static string path = AppDomain.CurrentDomain.BaseDirectory;
        private static string dbPath = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename="+path+@"App_Data\WebServiceBD.mdf;Integrated Security = True";
        private SqlConnection Connection = new SqlConnection(connectionString: dbPath);
        private SqlDataAdapter daUsers, daAdmins;
        private DataSet dsUsers, dsAdmins;

        private DataSet findEmails(string email, bool isAdmin)
        {
            Console.WriteLine(dbPath);
            DataSet emails = new DataSet();


            if (isAdmin)
            {
                Connection.Open();
                daAdmins = new SqlDataAdapter("SELECT * FROM Administratori WHERE Email LIKE @emailval", Connection);
                daAdmins.SelectCommand.Parameters.AddWithValue("@emailval", email);
                daAdmins.Fill(emails, "Admini");
                Connection.Close();
            }

            else
            {
                Connection.Open();
                daUsers = new SqlDataAdapter("SELECT * FROM Useri WHERE Email LIKE @emailval", Connection);
                daUsers.SelectCommand.Parameters.AddWithValue("@emailval", email);
                daUsers.Fill(emails, "Useri");
                Connection.Close();
            }

            return emails;
        }

        private int getLastId(string databaseName)
        {
            Connection.Open();

            int lastID = 0;

            if (databaseName.Equals("Useri"))
            {
                dsUsers = new DataSet();
                daUsers = new SqlDataAdapter("SELECT TOP 1 * FROM Useri ORDER BY Id DESC", Connection);
                daUsers.Fill(dsUsers, "Users");


                foreach (DataRow dr in dsUsers.Tables["Users"].Rows)
                {
                    bool success = int.TryParse(dr.ItemArray.GetValue(0).ToString(), out lastID);
                    if (success)
                    {
                        lastID++;
                    }
                    else
                    {
                        lastID = 0;
                    }
                }


                Connection.Close();
                return lastID;
            }
            else
            {
                dsAdmins = new DataSet();
                daAdmins = new SqlDataAdapter("SELECT TOP 1 * FROM Administratori ORDER BY Id DESC", Connection);
                daAdmins.Fill(dsAdmins, "Admini");

                foreach (DataRow dr in dsAdmins.Tables["Admini"].Rows)
                {
                    bool success = int.TryParse(dr.ItemArray.GetValue(0).ToString(), out lastID);
                    if (success)
                    {
                        lastID++;
                    }
                    else
                    {
                        lastID = 0;
                    }
                }


                Connection.Close();
                return lastID;
            }
        }

        [WebMethod]
        public bool createUserAccount(string nume, string prenume, string email, string parola, string telefon, string adresa, byte[] byteArrayImage)
        {
            bool success = false;
            DataSet checkEmailSet = findEmails(email, false);
            if (checkEmailSet.Tables["Useri"].Rows.Count == 0)
            {
                int Id = getLastId("Useri");
                Connection.Open();
 
                SqlCommand Cmd = new SqlCommand("INSERT INTO Useri(Id, Nume, Prenume, Email, Parola, Telefon, Adresa, NrAnunturi, IsBanned, IsReported, CodAnuntRaportat, Observatii, ImaginePortret) " +
                    "VALUES (@valId, @valNume, @valPrenume, @valEmail, @valParola, @valTelefon, @valAdresa, @valNrAnunturi, @valIsBanned, @valIsReported, @valCodAnuntRaportat, @valObservatii, @valImaginePortret)", Connection);
         
              
                string obs = string.Empty;
                Cmd.Parameters.AddWithValue("@valId", Id);
                Cmd.Parameters.AddWithValue("@valNume", nume);
                Cmd.Parameters.AddWithValue("@valPrenume", prenume);
                Cmd.Parameters.AddWithValue("@valEmail", email);
                Cmd.Parameters.AddWithValue("@valParola", parola);
                Cmd.Parameters.AddWithValue("@valTelefon", telefon);
                Cmd.Parameters.AddWithValue("@valAdresa", adresa);
                Cmd.Parameters.AddWithValue("@valNrAnunturi", 0);
                Cmd.Parameters.AddWithValue("@valIsBanned",0);
                Cmd.Parameters.AddWithValue("@valIsReported",0);
                Cmd.Parameters.AddWithValue("@valCodAnuntRaportat", -1);
                Cmd.Parameters.AddWithValue("@valObservatii", "");
                Cmd.Parameters.AddWithValue("@valImaginePortret", byteArrayImage);
                Cmd.ExecuteNonQuery();

                Connection.Close();
                success = true;
            }
            else
            {
                //email existent
                return success;
            }

            return success;
        }

        [WebMethod]
        public bool createAdminAccount(string nume, string prenume, string email, string parola, string contact, byte[] byteArrayImage)
        {
            bool success = false;
            DataSet checkEmailSet = findEmails(email, true);
            if (checkEmailSet.Tables["Admini"].Rows.Count == 0)
            {
                int Id = getLastId("Admini");
                Connection.Open();

                SqlCommand Cmd = new SqlCommand("INSERT INTO Administratori(Id, Nume, Prenume, Email, Parola, Contact, Observatii, ImaginePortret) " +
                    "VALUES (@valId, @valNume, @valPrenume, @valEmail, @valParola, @valContact, @valObservatii, @valImaginePortret)", Connection);


                string obs = string.Empty;
                Cmd.Parameters.AddWithValue("@valId", Id);
                Cmd.Parameters.AddWithValue("@valNume", nume);
                Cmd.Parameters.AddWithValue("@valPrenume", prenume);
                Cmd.Parameters.AddWithValue("@valEmail", email);
                Cmd.Parameters.AddWithValue("@valParola", parola);
                Cmd.Parameters.AddWithValue("@valContact", contact);
                Cmd.Parameters.AddWithValue("@valObservatii", "");
                Cmd.Parameters.AddWithValue("@valImaginePortret", byteArrayImage);
                Cmd.ExecuteNonQuery();

                Connection.Close();
                success = true;
            }
            else
            {
                Console.WriteLine("Email deja existent!");
                return success;
            }

            return success;
        }

    }
}
