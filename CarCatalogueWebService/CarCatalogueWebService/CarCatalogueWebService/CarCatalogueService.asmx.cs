﻿using CarCatalogueWebService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CarCatalogueWebService
{
    /// <summary>
    /// Summary description for CarCatalogueService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CarCatalogueService : System.Web.Services.WebService
    {

        private static string startPath = System.IO.Directory.GetCurrentDirectory();
        static string path = AppDomain.CurrentDomain.BaseDirectory;
        private static string dbPath = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + path + @"App_Data\WebServiceBD.mdf;Integrated Security = True";
        private SqlConnection Connection = new SqlConnection(connectionString: dbPath);
        private SqlDataAdapter daAnunturi;
        private DataSet dsAnunturi;
        AdminHandler admin;

        [WebMethod]
        public System.Drawing.Image ConvertByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray);
            Image rez = Image.FromStream(ms);
            return rez;
        }

        [WebMethod] 
        public DataSet MyAnunturi(string userid)
        {
            Connection.Open();
            dsAnunturi = new DataSet();
            daAnunturi = new SqlDataAdapter("SELECT * FROM Anunturi WHERE Id = @valUserId ", Connection);
            daAnunturi.SelectCommand.Parameters.AddWithValue("@valUserId", userid);
            daAnunturi.Fill(dsAnunturi, "Anunturi");
            Connection.Close();
            return dsAnunturi;
           
        }

        [WebMethod]
        public DataSet PopulateAnunturi(string marca, string model, string pretmin, string pretmax, string varianta, string combustibil, 
            string anfabrmin, string anfabrmax, string ccmin, string ccmax, string cpmin, string cpmax, string kmmin, string kmmax, 
            string caroserie, string culoare, string cutieviteze, string locatie, string searchtext)
        {
           Connection.Open();

           dsAnunturi = new DataSet();
           daAnunturi = new SqlDataAdapter("SELECT * FROM Anunturi WHERE " +
                                                                   "Marca LIKE @valMarca " +
                                                                   "AND Model LIKE @valModel " +
                                                                   "AND Varianta LIKE @valVarianta " +
                                                                   "AND Combustibil LIKE @valCombustibil " +
                                                                   "AND Caroserie LIKE @valCaroserie " +
                                                                   "AND Culoare LIKE @valCuloare " +
                                                                   "AND CutieDeViteze LIKE @valCutie " +
                                                                   "AND PretDeVanzare BETWEEN @valPretmin AND @valPretmax " +
                                                                   "AND AnPrimaInmatriculare BETWEEN @valAnFabrmin AND @valAnFabrmax " +
                                                                   "AND CapacitateCilindrica BETWEEN @valCCmin AND @valCCmax " +
                                                                   "AND Putere BETWEEN @valPmin AND @valPmax " +
                                                                   "AND Kilometri BETWEEN @valKmmin AND @valKmmax " +
                                                                   "AND Locatie LIKE @valLocatie ", Connection);
          
          /*  if (searchtext.Equals("")) daAnunturi.SelectCommand.Parameters.AddWithValue("@valSearchText", "%%");
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valSearchText", "%" + searchtext + "%"); */


            if (marca.Equals("Neselectat"))
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valMarca", "%%");
            else
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valMarca", "%" + marca + "%");

            if (model.Equals("Neselectat"))
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valModel", "%%");
            else
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valModel", "%" + model + "%"); 

            if (varianta.Equals("Neselectat"))
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valVarianta", "%%");
            else
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valVarianta", "%" + varianta + "%");

            if (combustibil.Equals("Neselectat"))
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valCombustibil", "%%");
            else
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valCombustibil", "%" + combustibil + "%");

            if (caroserie.Equals("Neselectat"))
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valCaroserie", "%%");
            else
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valCaroserie", "%" + caroserie + "%");

            if (culoare.Equals("Neselectat"))
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valCuloare", "%%");
            else
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valCuloare", "%" + culoare + "%");

            if (cutieviteze.Equals("Neselectat"))
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valCutie", "%%");
            else
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valCutie", "%" + cutieviteze + "%");

            if (locatie.Equals("Neselectat"))
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valLocatie", "%%");
            else
                daAnunturi.SelectCommand.Parameters.AddWithValue("@valLocatie", "%" + locatie + "%");

            if (Int32.Parse(kmmin) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valKmmin", 0);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valKmmin", Int32.Parse(kmmin));

            if (Int32.Parse(kmmax) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valKmmax", int.MaxValue);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valKmmax", Int32.Parse(kmmax));

            if (Int32.Parse(cpmin) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valPmin", 0);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valPmin", Int32.Parse(cpmin));

            if (Int32.Parse(cpmax) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valPmax", 32767);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valPmax", Int32.Parse(cpmax));

            if (Int32.Parse(ccmin) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valCCmin", 0);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valCCmin", Int32.Parse(ccmin));

            if (Int32.Parse(ccmax) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valCCmax", 32767);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valCCmax", Int32.Parse(ccmax));

            if (Int32.Parse(anfabrmin) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valAnFabrmin", 0);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valAnFabrmin", anfabrmin);

            if (Int32.Parse(anfabrmax) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valAnFabrmax", 2020);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valAnFabrmax", Int32.Parse(anfabrmax));

            if (Int32.Parse(pretmin) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valPretmin", 0);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valPretmin", Int32.Parse(pretmin));

            if (Int32.Parse(pretmax) == 0) daAnunturi.SelectCommand.Parameters.AddWithValue("@valPretmax", int.MaxValue);
            else daAnunturi.SelectCommand.Parameters.AddWithValue("@valPretmax", Int32.Parse(pretmax));

            daAnunturi.Fill(dsAnunturi, "Anunturi");
         
                
            Connection.Close();

            return dsAnunturi;
        }

    }
}
