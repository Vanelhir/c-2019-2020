﻿using CarCatalogueWebClient.LoginServiceReference;
using CarCatalogueWebClient.ViewAnnounceService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{
    public partial class ViewAnuntForm : Form
    {
        AnnounceHandler anunt;
        bool bidable = false;
        UserHandler user;

        CarCatalogueWebClient.ViewAnnounceService.ViewAnuntServiceSoapClient ViewAnuntService
            = new CarCatalogueWebClient.ViewAnnounceService.ViewAnuntServiceSoapClient();

        public ViewAnuntForm(AnnounceHandler anunt, UserHandler user)
        {
            this.anunt = anunt;
            this.user = user;
            InitializeComponent();
            init();
        }

        private void init()
        {
            labelMarca.Text = "Marca: " + anunt.marca;
            labelModel.Text = "Model: " + anunt.model;
            labelVarianta.Text = "Varianta: " + anunt.varianta;
            
            if(anunt.an == 1) labelAn.Text = "An fabricatie: nespecificat.";
            else labelAn.Text = "An fabricatie: " + anunt.an;
            if (anunt.km == 1) labelkm.Text = "Km: nespecificat.";
            else labelkm.Text = "Km: " + anunt.km;
            if (anunt.cc == 1) labelCC.Text = "CC: nespecificat.";
            else labelCC.Text = "CC: " + anunt.cc;
            
            labelCutie.Text = "Cutie de viteze: " + anunt.cutviteze;

            if (anunt.cp == 1) labelCP.Text = "CP: nespecificat.";
            else labelCP.Text = "CP: " + anunt.cp;
            if (anunt.kw == 1) labelKW.Text = "kW: nespecificat.";
            else labelKW.Text = "kW: " + anunt.kw;
            
            labelCombustibil.Text = "Combustibil: " + anunt.combustibil;
            labelCuloare.Text = "Culoare: " + anunt.culoare;
            labelCaroserie.Text = "Caroserie: " + anunt.caroserie;

            if (anunt.pretLicitatie == 1) labelPretLicitatieCurenta.Text = "Licitatie curenta: nu se poate licita!";
            else labelPretLicitatieCurenta.Text = "Licitatie Curenta: " + anunt.pretLicitatie + "     ID licitant: "+anunt.lastlic;
            if (anunt.pretVanzare == 1) labelPretLicitatieCurenta.Text = "Pret: oferit exclusiv prin licitatie!";
            else labelPretVanzare.Text = "Pret: " + anunt.pretVanzare;
            
            labelLocatie.Text = "Locatie: " + anunt.locatie;
            labelNumeVanzator.Text = "Vanzator: " + anunt.numeuser + " " + anunt.prenumeuser;
            labelTelefonVanzator.Text = "Telefon: " + anunt.telefonuser;
            labelCod.Text = "Cod anunt: " + anunt.codanunt;


            pictureBoxVanzator.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage; 
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage; 
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;

            pictureBoxVanzator.Image = ConvertByteArrayToImage(anunt.imaguser);
            pictureBox1.Image = ConvertByteArrayToImage(anunt.imagAnunt);
            pictureBox2.Image = ConvertByteArrayToImage(anunt.im1);
            pictureBox3.Image = ConvertByteArrayToImage(anunt.im2);
            pictureBox4.Image = ConvertByteArrayToImage(anunt.im3);

            richTextBoxDescriere.Text = anunt.descriere;
            if (anunt.persoana.Equals("Persoana fizica")) checkBox1.Checked = true;
            else checkBox2.Checked = true;
        }

        private System.Drawing.Image ConvertByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray);
            System.Drawing.Image rez = System.Drawing.Image.FromStream(ms);
            return rez;
        }

        private void buttonLiciteaza_Click(object sender, EventArgs e)
        {
            if (anunt.pretLicitatie == 1)
            {
                //nu se poate licita
                return;
            }
            if(user.id == anunt.id)
            {
                //nu se poate licita de persoana creatoare a anuntului
                return;
            }
            else
            {
                if(int.TryParse(textBoxLicitatie.Text, out int licCurenta))
                {
                    if (licCurenta <= anunt.pretLicitatie)
                    {
                        //licitatia trebuie sa fie mai mare decat cea curenta
                        return;
                    }
                    else
                    {
                        ViewAnuntService.updateLic(anunt.codanunt, licCurenta, user.id);
                        init();
                        labelPretLicitatieCurenta.Text = "Licitatie Curenta: " + licCurenta + "     ID licitant: " + user.id;
                    }
                }
            }
        }

        private void buttonReport_Click(object sender, EventArgs e)
        {
            ViewAnuntService.updateReport(anunt.id, anunt.codanunt);
        }

        private void buttonInchidere_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
