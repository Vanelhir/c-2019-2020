﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{


    public partial class CreateAccountForm : Form
    {
        CarCatalogueWebClient.CreateAccountServiceReference.CreateAccountServiceSoapClient createAccountService
            = new CarCatalogueWebClient.CreateAccountServiceReference.CreateAccountServiceSoapClient();

        private string imageLocation;
        private string enteredLastName;
        private string enteredFirstName;
        private string enteredEmail;
        private string enteredPassw;
        private string enteredRePass;
        private string enteredPhNmb;
        private string enteredAdress;
        private string enteredAdmContact;
        private string enteredMasterPass;
        private string masterPassword = "p4r014t";
        private bool isAdmin = false;
        private LoginForm lf;

        public CreateAccountForm(LoginForm lf)
        {
            this.lf = lf;
            InitializeComponent();
            textBoxAdresa.Enabled = true;
            textBoxTelefon.Enabled = true;
            textBoxContactAdmin.Enabled = false;
            textBoxMasterPassword.Enabled = false;
            lf.Visible = false;
            textBoxMasterPassword.PasswordChar = '*';
            textBoxParola.PasswordChar = '*';
            textBoxParolaRe.PasswordChar = '*';
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFD = new OpenFileDialog();

            if (oFD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                imageLocation = oFD.FileName;
                pictureBox1.Image = Image.FromFile(imageLocation);
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private byte[] ConvertImageToByteArray (System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            byte[] rez;

            try
            {
                using(MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, format);
                    rez = ms.ToArray();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return rez;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //create account
            if (!isAdmin)
            {
                enteredLastName = textBoxNume.Text;
                enteredFirstName = textBoxPrenume.Text;
                enteredEmail = textBoxEmail.Text;
                enteredPassw = textBoxParola.Text;
                enteredRePass = textBoxParolaRe.Text;
                enteredPhNmb = textBoxTelefon.Text;
                enteredAdress = textBoxAdresa.Text;

                if (String.IsNullOrEmpty(enteredFirstName))
                {
                    //prenumele nu poate fi nul
                    new ErrorForm(4);
                    return;
                }

                if (String.IsNullOrEmpty(enteredLastName))
                {
                    //numele nu poate fi nul
                    new ErrorForm(5);
                    return;
                }

                if (String.IsNullOrEmpty(enteredEmail))
                {
                    //emailul nu poate fi nul
                    new ErrorForm(6);
                    return;
                }

                if (String.IsNullOrEmpty(enteredPassw))
                {
                    //parola nu poate fi nula
                    new ErrorForm(7);
                    return;
                }

                if(!enteredPassw.Equals(enteredRePass))
                {
                    //parolele trebuie sa fie la fel
                    new ErrorForm(8);
                    return;
                }

                if (String.IsNullOrEmpty(enteredPhNmb))
                {
                    enteredPhNmb = String.Empty;
                }

                if (String.IsNullOrEmpty(enteredAdress))
                {
                    enteredAdress = String.Empty;
                }

                byte[] byteArrayAccImage = ConvertImageToByteArray(pictureBox1.Image, System.Drawing.Imaging.ImageFormat.Png);

                bool success = createAccountService.createUserAccount(enteredLastName, enteredFirstName, enteredEmail, enteredPassw, enteredPhNmb, enteredAdress, byteArrayAccImage);
                if (success) Console.WriteLine("User creat");
                else Console.WriteLine("Nu sa putut crea userul");
            }

            else
            {
                enteredLastName = textBoxNume.Text;
                enteredFirstName = textBoxPrenume.Text;
                enteredEmail = textBoxEmail.Text;
                enteredPassw = textBoxParola.Text;
                enteredRePass = textBoxParolaRe.Text;
                enteredAdmContact = textBoxContactAdmin.Text;
                enteredMasterPass = textBoxMasterPassword.Text;

                if (!enteredMasterPass.Equals(masterPassword))
                {
                    return;
                }

                if (String.IsNullOrEmpty(enteredFirstName))
                {
                    new ErrorForm(4);
                    return;
                }

                if (String.IsNullOrEmpty(enteredLastName))
                {
                    //numele nu poate fi nul
                    new ErrorForm(5);
                    return;
                }

                if (String.IsNullOrEmpty(enteredEmail))
                {
                    //emailul nu poate fi nul
                    new ErrorForm(6);
                    return;
                }

                if (String.IsNullOrEmpty(enteredPassw))
                {
                    //parola nu poate fi nula
                    new ErrorForm(7);
                    return;
                }

                if (String.IsNullOrEmpty(enteredAdmContact))
                {
                    enteredAdmContact = string.Empty;
                    return;
                }

                if (!enteredPassw.Equals(enteredRePass))
                {
                    new ErrorForm(8);
                    return;
                }
        

                byte[] byteArrayAccImage = ConvertImageToByteArray(pictureBox1.Image, System.Drawing.Imaging.ImageFormat.Png);

                bool success = createAccountService.createAdminAccount(enteredLastName, enteredFirstName, enteredEmail, enteredPassw, enteredAdmContact, byteArrayAccImage);
                if (success) Console.WriteLine("Admin creat");
                else Console.WriteLine("Nu sa putut crea Adminul");
            }
            lf.Visible = true;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lf.Visible = true;
            this.Close();
        }

        private void checkBoxAdmin_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAdmin.Checked)
            {
                isAdmin = true;
                textBoxAdresa.Enabled = false;
                textBoxTelefon.Enabled = false;
                textBoxContactAdmin.Enabled = true;
                textBoxMasterPassword.Enabled = true;
            }
            else
            {
                isAdmin = false;
                textBoxAdresa.Enabled = true;
                textBoxTelefon.Enabled = true;
                textBoxContactAdmin.Enabled = false;
                textBoxMasterPassword.Enabled = false;
            }
        }
    }
}
