﻿using System;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{
    public partial class ErrorForm : Form
    {
        int code;
        public ErrorForm(int code)
        {
            InitializeComponent();
            this.code = code;
            ShowError(code);
            this.Show();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        public void ShowError(int ErrorCode)
        {
            switch (ErrorCode)
            {
                case 1:
                    ErrorText.Text = "Eroare: Parola incorecta!";
                    break;

                case 2:
                    ErrorText.Text = "Eroare: Emailul nu exista in baza de date!";
                    break;

                case 3:
                    ErrorText.Text = "Eroare: Emailul nu este in forma corecta!";
                    break;

                case 4:
                    ErrorText.Text = "Eroare: Prenumele nu poate fi nul!";
                    break;

                case 5:
                    ErrorText.Text = "Eroare: Numele nu poate fi nul!";
                    break;

                case 6:
                    ErrorText.Text = "Eroare: Emailul nu poate fi nul!";
                    break;

                case 7:
                    ErrorText.Text = "Eroare: Parola nu poate fi nula!";
                    break;

                case 8:
                    ErrorText.Text = "Eroare: Parolele nu sunt aceleasi!";
                    break;

                case 9:
                    ErrorText.Text = "Eroare: Pretul trebuie sa fie un numar!";
                    break;

                case 10:
                    ErrorText.Text = "Eroare: Pretul de start al licitatiei trebuie sa fie un numar!";
                    break;

                case 11:
                    ErrorText.Text = "Eroare: Pretul de start al licitatiei trebuie sa fie mai mic ca pretul de vanzare!";
                    break;

                case 12:
                    ErrorText.Text = "Eroare: Anul de fabricatie trebuie sa fie un numar!";
                    break;

                case 13:
                    ErrorText.Text = "Eroare: Numarul de kilometrii trebuie sa fie o valoare numerica!";
                    break;

                case 14:
                    ErrorText.Text = "Eroare: Puterea (CP) trebuie sa fie o valoare numerica!";
                    break;

                case 15:
                    ErrorText.Text = "Eroare: Puterea (kW) trebuie sa fie o valoare numerica!";
                    break;

                case 16:
                    ErrorText.Text = "Eroare: Capacitatea cilindrica trebuie sa fie o valoare numerica!";
                    break;

                case 17:
                    ErrorText.Text = "Eroare: Contul tau a fost suspendat!";
                    break;

                default:
                    Console.WriteLine("Cod incorect");
                    break;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
