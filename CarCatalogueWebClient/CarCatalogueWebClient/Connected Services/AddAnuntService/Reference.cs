﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarCatalogueWebClient.AddAnuntService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="AddAnuntService.AddAnuntServiceSoap")]
    public interface AddAnuntServiceSoap {
        
        // CODEGEN: Generating message contract since element name caroserie from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/addAnuntLaDataBase", ReplyAction="*")]
        CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseResponse addAnuntLaDataBase(CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/addAnuntLaDataBase", ReplyAction="*")]
        System.Threading.Tasks.Task<CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseResponse> addAnuntLaDataBaseAsync(CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequest request);
        
        // CODEGEN: Generating message contract since element name caroserie from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/updateAnuntInDataBase", ReplyAction="*")]
        CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseResponse updateAnuntInDataBase(CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/updateAnuntInDataBase", ReplyAction="*")]
        System.Threading.Tasks.Task<CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseResponse> updateAnuntInDataBaseAsync(CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class addAnuntLaDataBaseRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="addAnuntLaDataBase", Namespace="http://tempuri.org/", Order=0)]
        public CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequestBody Body;
        
        public addAnuntLaDataBaseRequest() {
        }
        
        public addAnuntLaDataBaseRequest(CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class addAnuntLaDataBaseRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int id;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string caroserie;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string marca;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string model;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string varianta;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public int pret;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int lic;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=7)]
        public int an;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=8)]
        public int km;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=9)]
        public int putere;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=10)]
        public int puterekw;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=11)]
        public string combustibil;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public string cutieviteze;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=13)]
        public int cc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=14)]
        public string culoare;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=15)]
        public string locatie;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=16)]
        public string persoana;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=17)]
        public string descriere;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=18)]
        public byte[] ima;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=19)]
        public byte[] im1;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=20)]
        public byte[] im2;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=21)]
        public byte[] im3;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=22)]
        public int licId;
        
        public addAnuntLaDataBaseRequestBody() {
        }
        
        public addAnuntLaDataBaseRequestBody(
                    int id, 
                    string caroserie, 
                    string marca, 
                    string model, 
                    string varianta, 
                    int pret, 
                    int lic, 
                    int an, 
                    int km, 
                    int putere, 
                    int puterekw, 
                    string combustibil, 
                    string cutieviteze, 
                    int cc, 
                    string culoare, 
                    string locatie, 
                    string persoana, 
                    string descriere, 
                    byte[] ima, 
                    byte[] im1, 
                    byte[] im2, 
                    byte[] im3, 
                    int licId) {
            this.id = id;
            this.caroserie = caroserie;
            this.marca = marca;
            this.model = model;
            this.varianta = varianta;
            this.pret = pret;
            this.lic = lic;
            this.an = an;
            this.km = km;
            this.putere = putere;
            this.puterekw = puterekw;
            this.combustibil = combustibil;
            this.cutieviteze = cutieviteze;
            this.cc = cc;
            this.culoare = culoare;
            this.locatie = locatie;
            this.persoana = persoana;
            this.descriere = descriere;
            this.ima = ima;
            this.im1 = im1;
            this.im2 = im2;
            this.im3 = im3;
            this.licId = licId;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class addAnuntLaDataBaseResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="addAnuntLaDataBaseResponse", Namespace="http://tempuri.org/", Order=0)]
        public CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseResponseBody Body;
        
        public addAnuntLaDataBaseResponse() {
        }
        
        public addAnuntLaDataBaseResponse(CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class addAnuntLaDataBaseResponseBody {
        
        public addAnuntLaDataBaseResponseBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateAnuntInDataBaseRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateAnuntInDataBase", Namespace="http://tempuri.org/", Order=0)]
        public CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequestBody Body;
        
        public updateAnuntInDataBaseRequest() {
        }
        
        public updateAnuntInDataBaseRequest(CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class updateAnuntInDataBaseRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public int code;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string caroserie;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string marca;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string model;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string varianta;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=5)]
        public int pret;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public int lic;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=7)]
        public int an;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=8)]
        public int km;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=9)]
        public int putere;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=10)]
        public int puterekw;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=11)]
        public string combustibil;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public string cutieviteze;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=13)]
        public int cc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=14)]
        public string culoare;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=15)]
        public string locatie;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=16)]
        public string persoana;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=17)]
        public string descriere;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=18)]
        public byte[] ima;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=19)]
        public byte[] im1;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=20)]
        public byte[] im2;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=21)]
        public byte[] im3;
        
        public updateAnuntInDataBaseRequestBody() {
        }
        
        public updateAnuntInDataBaseRequestBody(
                    int code, 
                    string caroserie, 
                    string marca, 
                    string model, 
                    string varianta, 
                    int pret, 
                    int lic, 
                    int an, 
                    int km, 
                    int putere, 
                    int puterekw, 
                    string combustibil, 
                    string cutieviteze, 
                    int cc, 
                    string culoare, 
                    string locatie, 
                    string persoana, 
                    string descriere, 
                    byte[] ima, 
                    byte[] im1, 
                    byte[] im2, 
                    byte[] im3) {
            this.code = code;
            this.caroserie = caroserie;
            this.marca = marca;
            this.model = model;
            this.varianta = varianta;
            this.pret = pret;
            this.lic = lic;
            this.an = an;
            this.km = km;
            this.putere = putere;
            this.puterekw = puterekw;
            this.combustibil = combustibil;
            this.cutieviteze = cutieviteze;
            this.cc = cc;
            this.culoare = culoare;
            this.locatie = locatie;
            this.persoana = persoana;
            this.descriere = descriere;
            this.ima = ima;
            this.im1 = im1;
            this.im2 = im2;
            this.im3 = im3;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class updateAnuntInDataBaseResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="updateAnuntInDataBaseResponse", Namespace="http://tempuri.org/", Order=0)]
        public CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseResponseBody Body;
        
        public updateAnuntInDataBaseResponse() {
        }
        
        public updateAnuntInDataBaseResponse(CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class updateAnuntInDataBaseResponseBody {
        
        public updateAnuntInDataBaseResponseBody() {
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface AddAnuntServiceSoapChannel : CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AddAnuntServiceSoapClient : System.ServiceModel.ClientBase<CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap>, CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap {
        
        public AddAnuntServiceSoapClient() {
        }
        
        public AddAnuntServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AddAnuntServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AddAnuntServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AddAnuntServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseResponse CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap.addAnuntLaDataBase(CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequest request) {
            return base.Channel.addAnuntLaDataBase(request);
        }
        
        public void addAnuntLaDataBase(
                    int id, 
                    string caroserie, 
                    string marca, 
                    string model, 
                    string varianta, 
                    int pret, 
                    int lic, 
                    int an, 
                    int km, 
                    int putere, 
                    int puterekw, 
                    string combustibil, 
                    string cutieviteze, 
                    int cc, 
                    string culoare, 
                    string locatie, 
                    string persoana, 
                    string descriere, 
                    byte[] ima, 
                    byte[] im1, 
                    byte[] im2, 
                    byte[] im3, 
                    int licId) {
            CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequest inValue = new CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequest();
            inValue.Body = new CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequestBody();
            inValue.Body.id = id;
            inValue.Body.caroserie = caroserie;
            inValue.Body.marca = marca;
            inValue.Body.model = model;
            inValue.Body.varianta = varianta;
            inValue.Body.pret = pret;
            inValue.Body.lic = lic;
            inValue.Body.an = an;
            inValue.Body.km = km;
            inValue.Body.putere = putere;
            inValue.Body.puterekw = puterekw;
            inValue.Body.combustibil = combustibil;
            inValue.Body.cutieviteze = cutieviteze;
            inValue.Body.cc = cc;
            inValue.Body.culoare = culoare;
            inValue.Body.locatie = locatie;
            inValue.Body.persoana = persoana;
            inValue.Body.descriere = descriere;
            inValue.Body.ima = ima;
            inValue.Body.im1 = im1;
            inValue.Body.im2 = im2;
            inValue.Body.im3 = im3;
            inValue.Body.licId = licId;
            CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseResponse retVal = ((CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap)(this)).addAnuntLaDataBase(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseResponse> CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap.addAnuntLaDataBaseAsync(CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequest request) {
            return base.Channel.addAnuntLaDataBaseAsync(request);
        }
        
        public System.Threading.Tasks.Task<CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseResponse> addAnuntLaDataBaseAsync(
                    int id, 
                    string caroserie, 
                    string marca, 
                    string model, 
                    string varianta, 
                    int pret, 
                    int lic, 
                    int an, 
                    int km, 
                    int putere, 
                    int puterekw, 
                    string combustibil, 
                    string cutieviteze, 
                    int cc, 
                    string culoare, 
                    string locatie, 
                    string persoana, 
                    string descriere, 
                    byte[] ima, 
                    byte[] im1, 
                    byte[] im2, 
                    byte[] im3, 
                    int licId) {
            CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequest inValue = new CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequest();
            inValue.Body = new CarCatalogueWebClient.AddAnuntService.addAnuntLaDataBaseRequestBody();
            inValue.Body.id = id;
            inValue.Body.caroserie = caroserie;
            inValue.Body.marca = marca;
            inValue.Body.model = model;
            inValue.Body.varianta = varianta;
            inValue.Body.pret = pret;
            inValue.Body.lic = lic;
            inValue.Body.an = an;
            inValue.Body.km = km;
            inValue.Body.putere = putere;
            inValue.Body.puterekw = puterekw;
            inValue.Body.combustibil = combustibil;
            inValue.Body.cutieviteze = cutieviteze;
            inValue.Body.cc = cc;
            inValue.Body.culoare = culoare;
            inValue.Body.locatie = locatie;
            inValue.Body.persoana = persoana;
            inValue.Body.descriere = descriere;
            inValue.Body.ima = ima;
            inValue.Body.im1 = im1;
            inValue.Body.im2 = im2;
            inValue.Body.im3 = im3;
            inValue.Body.licId = licId;
            return ((CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap)(this)).addAnuntLaDataBaseAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseResponse CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap.updateAnuntInDataBase(CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequest request) {
            return base.Channel.updateAnuntInDataBase(request);
        }
        
        public void updateAnuntInDataBase(
                    int code, 
                    string caroserie, 
                    string marca, 
                    string model, 
                    string varianta, 
                    int pret, 
                    int lic, 
                    int an, 
                    int km, 
                    int putere, 
                    int puterekw, 
                    string combustibil, 
                    string cutieviteze, 
                    int cc, 
                    string culoare, 
                    string locatie, 
                    string persoana, 
                    string descriere, 
                    byte[] ima, 
                    byte[] im1, 
                    byte[] im2, 
                    byte[] im3) {
            CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequest inValue = new CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequest();
            inValue.Body = new CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequestBody();
            inValue.Body.code = code;
            inValue.Body.caroserie = caroserie;
            inValue.Body.marca = marca;
            inValue.Body.model = model;
            inValue.Body.varianta = varianta;
            inValue.Body.pret = pret;
            inValue.Body.lic = lic;
            inValue.Body.an = an;
            inValue.Body.km = km;
            inValue.Body.putere = putere;
            inValue.Body.puterekw = puterekw;
            inValue.Body.combustibil = combustibil;
            inValue.Body.cutieviteze = cutieviteze;
            inValue.Body.cc = cc;
            inValue.Body.culoare = culoare;
            inValue.Body.locatie = locatie;
            inValue.Body.persoana = persoana;
            inValue.Body.descriere = descriere;
            inValue.Body.ima = ima;
            inValue.Body.im1 = im1;
            inValue.Body.im2 = im2;
            inValue.Body.im3 = im3;
            CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseResponse retVal = ((CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap)(this)).updateAnuntInDataBase(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseResponse> CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap.updateAnuntInDataBaseAsync(CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequest request) {
            return base.Channel.updateAnuntInDataBaseAsync(request);
        }
        
        public System.Threading.Tasks.Task<CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseResponse> updateAnuntInDataBaseAsync(
                    int code, 
                    string caroserie, 
                    string marca, 
                    string model, 
                    string varianta, 
                    int pret, 
                    int lic, 
                    int an, 
                    int km, 
                    int putere, 
                    int puterekw, 
                    string combustibil, 
                    string cutieviteze, 
                    int cc, 
                    string culoare, 
                    string locatie, 
                    string persoana, 
                    string descriere, 
                    byte[] ima, 
                    byte[] im1, 
                    byte[] im2, 
                    byte[] im3) {
            CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequest inValue = new CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequest();
            inValue.Body = new CarCatalogueWebClient.AddAnuntService.updateAnuntInDataBaseRequestBody();
            inValue.Body.code = code;
            inValue.Body.caroserie = caroserie;
            inValue.Body.marca = marca;
            inValue.Body.model = model;
            inValue.Body.varianta = varianta;
            inValue.Body.pret = pret;
            inValue.Body.lic = lic;
            inValue.Body.an = an;
            inValue.Body.km = km;
            inValue.Body.putere = putere;
            inValue.Body.puterekw = puterekw;
            inValue.Body.combustibil = combustibil;
            inValue.Body.cutieviteze = cutieviteze;
            inValue.Body.cc = cc;
            inValue.Body.culoare = culoare;
            inValue.Body.locatie = locatie;
            inValue.Body.persoana = persoana;
            inValue.Body.descriere = descriere;
            inValue.Body.ima = ima;
            inValue.Body.im1 = im1;
            inValue.Body.im2 = im2;
            inValue.Body.im3 = im3;
            return ((CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoap)(this)).updateAnuntInDataBaseAsync(inValue);
        }
    }
}
