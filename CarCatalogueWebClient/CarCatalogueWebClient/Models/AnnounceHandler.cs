﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarCatalogueWebService.Models
{

 
    public class AnnounceHandler
    {

        public int id;
        public int codanunt;
        public string caroserie;
        public string marca;
        public string model;
        public string varianta;
        public int pretVanzare;
        public int pretLicitatie;
        public int an;
        public int km;
        public int cp;
        public int kw;
        public string combustibil;
        public string cutviteze;
        public int cc;
        public string culoare;
        public string dataadaugare;
        public string locatie;
        public string persoana;
        public string descriere;
        public byte[] imagAnunt, im1, im2, im3, imaguser;
        public string numeuser;
        public string prenumeuser;
        public string telefonuser;

        public AnnounceHandler()
        {

        }

        public AnnounceHandler(int id, int codanunt, string caroserie, string marca, string model, string varianta, int pretVanzare, int pretLicitatie, int an, int km, int cp, int kw, string combustibil, string cutviteze, int cc, string culoare, string dataadaugare, string locatie, string persoana, string descriere, byte[] imagAnunt, byte[] im1, byte[] im2, byte[] im3, byte[] imaguser, string numeuser, string prenumeuser, string telefonuser)
        {
            this.id = id;
            this.codanunt = codanunt;
            this.caroserie = caroserie;
            this.marca = marca;
            this.model = model;
            this.varianta = varianta;
            this.pretVanzare = pretVanzare;
            this.pretLicitatie = pretLicitatie;
            this.an = an;
            this.km = km;
            this.cp = cp;
            this.kw = kw;
            this.combustibil = combustibil;
            this.cutviteze = cutviteze;
            this.cc = cc;
            this.culoare = culoare;
            this.dataadaugare = dataadaugare;
            this.locatie = locatie;
            this.persoana = persoana;
            this.descriere = descriere;
            this.imagAnunt = imagAnunt;
            this.im1 = im1;
            this.im2 = im2;
            this.im3 = im3;
            this.imaguser = imaguser;
            this.numeuser = numeuser;
            this.prenumeuser = prenumeuser;
            this.telefonuser = telefonuser;
        }
    }
}