﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarCatalogueWebService.Models
{
    public class AdminHandler
    {
        public int id;
        public string nume;
        public string prenume;
        public string email;
        public string contact;
        public string observatii;
        public byte[] ImaginePortret;

        public AdminHandler(int id, string nume, string prenume, string email, string contact, string observatii, byte[] imaginePortret)
        {
            this.id = id;
            this.nume = nume;
            this.prenume = prenume;
            this.email = email;
            this.contact = contact;
            this.observatii = observatii;
            ImaginePortret = imaginePortret;
        }
    }
}