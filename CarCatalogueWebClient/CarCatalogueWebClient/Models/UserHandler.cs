﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace CarCatalogueWebService.Models
{
    public class UserHandler
    {
        public int id, nrAnunturi, codAnuntRaportat;
        public string nume, prenume, email, adresa, telefon, observatii;
        public bool isBanned, isReported;
        public byte[] byteImage;

        public UserHandler()
        {

        }

        public UserHandler(int id, int nrAnunturi, int codAnuntRaportat, string nume, string prenume, 
            string email, string adresa, string telefon, string observatii, bool isBanned, bool isReported,
            byte[] byteImage)
        {
            this.id = id;
            this.nrAnunturi = nrAnunturi;
            this.codAnuntRaportat = codAnuntRaportat;
            this.nume = nume;
            this.prenume = prenume;
            this.email = email;
            this.adresa = adresa;
            this.telefon = telefon;
            this.observatii = observatii;
            this.isBanned = isBanned;
            this.isReported = isReported;
            this.byteImage = byteImage;
        }
              
    }
}