﻿using CarCatalogueWebClient.LoginServiceReference;
using CarCatalogueWebClient.ViewAnnounceService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{
    public partial class AddAnuntForm : Form
    {

        CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoapClient addAnuntService =
            new CarCatalogueWebClient.AddAnuntService.AddAnuntServiceSoapClient();

        CarCatalogueWebClient.ViewAnnounceService.ViewAnuntServiceSoapClient ViewAnuntService
            = new CarCatalogueWebClient.ViewAnnounceService.ViewAnuntServiceSoapClient();

        UserHandler loggedInUser;
        private string imageLocation;
        byte[] byteImgAnunt, byteImg1, byteImg2, byteImg3;
        bool firma = false;
 
        AnnounceHandler anunt;


        public AddAnuntForm(UserHandler user)
        {
                InitializeComponent();
                loggedInUser = user;
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
                checkBox1.Checked = true;
            initForm();
        }

        public AddAnuntForm(UserHandler user, AnnounceHandler anunt)
        {

                InitializeComponent();
                this.anunt = anunt;
                loggedInUser = user;
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
                checkBox1.Checked = true;
                initForm2();
                button5.Text = "Modifica";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            readImage(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            readImage(2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            readImage(3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            readImage(4);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                firma = false;
                checkBox2.Checked = false;
                checkBox1.Checked = true;
            }
            else
            {
                firma = true;
                checkBox1.Checked = false;
                checkBox2.Checked = true;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                firma = true;
                checkBox1.Checked = false;
                checkBox2.Checked = true;
            }
            else
            {
                firma = false;
                checkBox1.Checked = true;
                checkBox2.Checked = false;
            }

        }



        private void readImage(int buton)
        {
            OpenFileDialog oFD = new OpenFileDialog();

            if (oFD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                imageLocation = oFD.FileName;
                System.Drawing.Image img = System.Drawing.Image.FromFile(imageLocation);
                if (buton == 1) pictureBox1.Image = img;
                if (buton == 2) pictureBox2.Image = img;
                if (buton == 3) pictureBox3.Image = img;
                if (buton == 4) pictureBox4.Image = img;
            }           
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(button5.Text.Equals("Adauga")) addAnuntToDataBase(false);
            else
            {
                addAnuntToDataBase(true);
            }
        }

        private void textBoxPutereCp_TextChanged(object sender, EventArgs e)
        {
            textBoxPuterekw.TextChanged -= textBoxPuterekw_TextChanged;

            if (int.TryParse(textBoxPutereCp.Text, out int cp)){
                textBoxPuterekw.Text = Math.Truncate(cp* 0.745).ToString();
            }
            else
            {
                // eroare nu se poate face int
            }

            textBoxPuterekw.TextChanged += textBoxPuterekw_TextChanged;
        }

        private void textBoxPuterekw_TextChanged(object sender, EventArgs e)
        {
            textBoxPutereCp.TextChanged -= textBoxPutereCp_TextChanged;

            if (int.TryParse(textBoxPuterekw.Text, out int kw))
            {
                textBoxPutereCp.Text = Math.Truncate(kw / 0.745).ToString();
            }
            else
            {
                // eroare nu se poate face int
            }

            textBoxPutereCp.TextChanged += textBoxPutereCp_TextChanged;
        }

        private byte[] ConvertImageToByteArray(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            byte[] rez;

            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, format);
                    rez = ms.ToArray();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return rez;
        }

        private void addAnuntToDataBase(bool update)
        {
            string marca = textBoxMarca.Text;
            string model = textBoxModel.Text;
            string varianta = textBoxVarianta.Text;
            string caroserie = comboBoxCaroserie.SelectedItem.ToString();
            string pretlic = textBoxPretlic.Text;
            string pretvan = textPretvanz.Text;
            string anfab = textBoxAn.Text;
            string km = textBoxKm.Text;
            string putereCp = textBoxPutereCp.Text;
            string putereKw = textBoxPuterekw.Text;
            string combustibil = comboBoxCombustibil.SelectedItem.ToString();
            string cc = textBoxCC.Text;
            string culoare = comboBoxCuloare.SelectedItem.ToString();
            string cutviteze = comboBoxCuloare.SelectedItem.ToString();
            string descriere = richTextBoxDescriere.Text;
            string locatie = textBoxLocatie.Text;

            byteImgAnunt = ConvertImageToByteArray(pictureBox1.Image, System.Drawing.Imaging.ImageFormat.Png);
            byteImg1 = ConvertImageToByteArray(pictureBox2.Image, System.Drawing.Imaging.ImageFormat.Png);
            byteImg2 = ConvertImageToByteArray(pictureBox3.Image, System.Drawing.Imaging.ImageFormat.Png);
            byteImg3 = ConvertImageToByteArray(pictureBox4.Image, System.Drawing.Imaging.ImageFormat.Png);

            int id = loggedInUser.id;
            if (string.IsNullOrEmpty(marca)) marca = "Neselectat";
            if (string.IsNullOrEmpty(model)) model = "Neselectat";
            if (string.IsNullOrEmpty(varianta)) varianta = "Neselectat";
            if (string.IsNullOrEmpty(locatie)) locatie = loggedInUser.adresa;

            if (!int.TryParse(pretlic, out int pretLicInt))
            {
                //Eroare nu se poate transforma pretul de licitatie din text in int
                new ErrorForm(9);
                return;
            }
            if(!int.TryParse(pretvan, out int pretVanInt))
            {
                //Eroare nu se poate transforma pretul de vanzare din text in int
                new ErrorForm(10);
                return;
            }
            if (pretVanInt < pretLicInt)
            {
                new ErrorForm(11);
                //Eroare pretul de licitatie este mai mare decat cel de vanzare
            }

            if (string.IsNullOrEmpty(anfab)) anfab = "1";
            else if(!int.TryParse(anfab, out int anFabInt))
            {
                //Eroare nu se poate transforma anul de fabricatie din text in int
                new ErrorForm(12);
                return;
            }
            if (string.IsNullOrEmpty(km)) km = "1";
            else if (!int.TryParse(km, out int kmInt))
            {
                //Eroare nu se poate transforma km din text in int
                new ErrorForm(13);
                return;
            }
            if (string.IsNullOrEmpty(putereCp)) putereCp = "1";
            else if (!int.TryParse(putereCp, out int cpInt))
            {
                //Eroare nu se poate transforma cp din text in int
                new ErrorForm(14);
                return;
            }
            if (string.IsNullOrEmpty(putereKw)) putereKw = "1";
            else if (!int.TryParse(putereKw, out int kwInt))
            {
                //Eroare nu se poate transforma kw din text in int
                new ErrorForm(15);
                return;
            }
            if (string.IsNullOrEmpty(cc)) cc = "1";
            else if (!int.TryParse(cc, out int ccInt))
            {
                //Eroare nu se poate transforma cc din text in int
                new ErrorForm(16);
                return;
            }
            string persoana = "Persoana fizica";
            if (firma) persoana = "Firma";
            if(!update)
            addAnuntService.addAnuntLaDataBase(id, caroserie, marca, model, varianta, int.Parse(pretvan), int.Parse(pretlic), int.Parse(anfab), int.Parse(km), int.Parse(putereCp), int.Parse(putereKw), combustibil, cutviteze, int.Parse(cc), culoare, locatie, persoana, descriere, byteImgAnunt, byteImg1, byteImg2, byteImg3, loggedInUser.id);
            else
            addAnuntService.updateAnuntInDataBase(anunt.codanunt, caroserie, marca, model, varianta, int.Parse(pretvan), int.Parse(pretlic), int.Parse(anfab), int.Parse(km), int.Parse(putereCp), int.Parse(putereKw), combustibil, cutviteze, int.Parse(cc), culoare, locatie, persoana, descriere, byteImgAnunt, byteImg1, byteImg2, byteImg3);
            Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Close();
        }

        public System.Drawing.Image ConvertByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray);
            System.Drawing.Image rez = System.Drawing.Image.FromStream(ms);
            return rez;
        }

        private void initForm()
        {

            comboBoxCaroserie.Items.Clear();
            comboBoxCaroserie.Items.Add("Nespecificat");
            comboBoxCaroserie.Items.Add("Sedan");
            comboBoxCaroserie.Items.Add("Combi");
            comboBoxCaroserie.Items.Add("Cabriolet");
            comboBoxCaroserie.Items.Add("Coupe");
            comboBoxCaroserie.Items.Add("Pick-up");
            comboBoxCaroserie.Items.Add("Off-road");
            comboBoxCaroserie.Items.Add("Alta caroserie");
            comboBoxCaroserie.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCaroserie.SelectedIndex = 0;

            comboBoxCuloare.Items.Clear();
            comboBoxCuloare.Items.Add("Nespecificat");
            comboBoxCuloare.Items.Add("Albastru");
            comboBoxCuloare.Items.Add("Rosu");
            comboBoxCuloare.Items.Add("Galben");
            comboBoxCuloare.Items.Add("Verde");
            comboBoxCuloare.Items.Add("Gri");
            comboBoxCuloare.Items.Add("Negru");
            comboBoxCuloare.Items.Add("Alb");
            comboBoxCuloare.Items.Add("Maro");
            comboBoxCuloare.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCuloare.SelectedIndex = 0;

            comboBoxCutVit.Items.Clear();
            comboBoxCutVit.Items.Add("Nespecificat");
            comboBoxCutVit.Items.Add("Manuala");
            comboBoxCutVit.Items.Add("Automata");
            comboBoxCutVit.Items.Add("Semi-automata");
            comboBoxCutVit.Items.Add("Secventiala");
            comboBoxCutVit.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCutVit.SelectedIndex = 0;

            comboBoxCombustibil.Items.Clear();
            comboBoxCombustibil.Items.Add("Nespecificat");
            comboBoxCombustibil.Items.Add("Benzina FTW");
            comboBoxCombustibil.Items.Add("Motorina");
            comboBoxCombustibil.Items.Add("GPL");
            comboBoxCombustibil.Items.Add("Hybrid");
            comboBoxCombustibil.Items.Add("Electric");
            comboBoxCombustibil.Items.Add("Altul");
            comboBoxCombustibil.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCombustibil.SelectedIndex = 0;
        }

        private void initForm2()
        {
            Console.WriteLine(anunt.marca);
            textBoxMarca.Text = anunt.marca;
            textBoxModel.Text = anunt.model;
            textBoxVarianta.Text = anunt.varianta;

            textBoxPretlic.Text = anunt.pretLicitatie.ToString();
            textPretvanz.Text = anunt.pretVanzare.ToString();
            textBoxAn.Text = anunt.an.ToString();
            textBoxKm.Text = anunt.km.ToString();
            textBoxPutereCp.Text = anunt.cp.ToString();
            textBoxPuterekw.Text = anunt.kw.ToString();

            textBoxCC.Text = anunt.cc.ToString(); ;

            richTextBoxDescriere.Text = anunt.descriere;
            textBoxLocatie.Text = anunt.locatie;

            pictureBox1.Image = ConvertByteArrayToImage(anunt.imagAnunt);
            pictureBox2.Image = ConvertByteArrayToImage(anunt.im1);
            pictureBox3.Image = ConvertByteArrayToImage(anunt.im2);
            pictureBox4.Image = ConvertByteArrayToImage(anunt.im3);

            initForm();
        }
    }
}
