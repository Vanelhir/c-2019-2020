﻿using CarCatalogueWebClient.LoginServiceReference;
using CarCatalogueWebClient.ViewAnnounceService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{
    public partial class CarCatalogueForm : Form
    {
        UserHandler loggedInUser;
        AdminHandler loggedInAdmin;
        DataSet dsAnunturi;
        List<String> poolAnFabr = new List<string>();


        CarCatalogueWebClient.CarCatalogueServiceReference.CarCatalogueServiceSoapClient CatalogueService
         = new CarCatalogueWebClient.CarCatalogueServiceReference.CarCatalogueServiceSoapClient();

        CarCatalogueWebClient.ViewAnnounceService.ViewAnuntServiceSoapClient ViewAnuntService
            = new CarCatalogueWebClient.ViewAnnounceService.ViewAnuntServiceSoapClient();

        public CarCatalogueForm(UserHandler user)
        {
            InitializeComponent();
            this.loggedInUser = user;
            System.Drawing.Image userImg = ConvertByteArrayToImage(user.byteImage); 
            this.pictureBox1.Image = userImg;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            LabelNumeUtilizator.Text = user.nume + " " + user.prenume;
            dsAnunturi = CatalogueService.PopulateAnunturi("Neselectat", "Neselectat", "0", "0", "Neselectat", "Neselectat", "0", "0", "0", "0", "0", "0", "0", "0", "Neselectat", "Neselectat", "Neselectat", "Neselectat", "ceva");
            this.dataGridView1.DataSource = dsAnunturi.Tables["Anunturi"].DefaultView;

            buttonAdminTools.Enabled = false;
            buttonAdminTools.Visible = false;
            init();
        }



        public CarCatalogueForm(AdminHandler admin)
        {
            InitializeComponent();
            this.loggedInAdmin = admin;
            System.Drawing.Image userImg = ConvertByteArrayToImage(admin.ImaginePortret);
            this.pictureBox1.Image = userImg;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            LabelNumeUtilizator.Text = admin.nume + " " + admin.prenume;
            dsAnunturi = CatalogueService.PopulateAnunturi("Neselectat", "Neselectat", "0", "0", "Neselectat", "Neselectat", "0", "0", "0", "0", "0", "0", "0", "0", "Neselectat", "Neselectat", "Neselectat", "Neselectat", "ceva");
            this.dataGridView1.DataSource = dsAnunturi.Tables["Anunturi"].DefaultView;

            butonAdaugaAnunt.Enabled = false;
            buttonAnunturileMele.Enabled = false;
            init();
        }

        private void init()
        {
            dataGridView1.Columns["Id"].Visible = false;
            dataGridView1.Columns["CodAnunt"].Visible = false;
            dataGridView1.Columns["Caroserie"].Visible = false;
            dataGridView1.Columns["PretDeStartLicitatie"].Visible = false;
            dataGridView1.Columns["PretDeVanzare"].Visible = false;
            dataGridView1.Columns["Putere"].Visible = false;
            dataGridView1.Columns["Kilometri"].Visible = false;
            dataGridView1.Columns["PutereKw"].Visible = false;
            dataGridView1.Columns["CutieDeViteze"].Visible = false;
            dataGridView1.Columns["Culoare"].Visible = false;
            dataGridView1.Columns["DataAdaugareAnunt"].Visible = false;
            dataGridView1.Columns["FirmaPersoanaFizica"].Visible = false;
            dataGridView1.Columns["Descriere"].Visible = false;
            dataGridView1.Columns["ImagineAnunt"].Visible = false;
            dataGridView1.Columns["Imagine1"].Visible = false;
            dataGridView1.Columns["Imagine2"].Visible = false;
            dataGridView1.Columns["Imagine3"].Visible = false;


            resetFilters();
        }
        public void resetFilters()
        {
            textBoxMarca.Text = "Nespecificat";
            textBoxModel.Text = "Nespecificat";
            textBoxPretmin.Text = "De la";
            textBoxPretmax.Text = "Pana la";
            textBoxVarianta.Text = "ex:GTI,RS,OPC,ST";
            textBoxPuteremin.Text = "De la";
            textBoxPuteremax.Text = "Pana la";
            textBoxKmmin.Text = "De la";
            textBoxKmmax.Text = "Pana la";
            textBoxLocatie.Text = "Nespecificat";

            comboBoxAn.Items.Clear();
            comboBoxAn.Items.Add("Nespecificat");
            comboBoxAn.Items.Add("< 1970");
            comboBoxAn.Items.Add("1971 - 1975");
            comboBoxAn.Items.Add("1976 - 1980");
            comboBoxAn.Items.Add("1981 - 1985");
            comboBoxAn.Items.Add("1986 - 1990");
            comboBoxAn.Items.Add("1991 - 1995");
            comboBoxAn.Items.Add("1996 - 2000");
            comboBoxAn.Items.Add("2001 - 2005");
            comboBoxAn.Items.Add("2006 - 2010");
            comboBoxAn.Items.Add("2011 - 2015");
            comboBoxAn.Items.Add("2016 - 2020");
            comboBoxAn.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxAn.SelectedIndex = 0;

            comboBoxCC.Items.Clear();
            comboBoxCC.Items.Add("Nespecificat");
            comboBoxCC.Items.Add("< 50 cc");
            comboBoxCC.Items.Add("51 - 200 cc");
            comboBoxCC.Items.Add("201 - 400 cc");
            comboBoxCC.Items.Add("401 - 600 cc");
            comboBoxCC.Items.Add("601 - 800 cc");
            comboBoxCC.Items.Add("801 - 1000 cc");
            comboBoxCC.Items.Add("1001 - 1400 cc");
            comboBoxCC.Items.Add("1401 - 1800 cc");
            comboBoxCC.Items.Add("1801 - 2200 cc");
            comboBoxCC.Items.Add("2201 - 3000 cc");
            comboBoxCC.Items.Add("> 3001 cc");
            comboBoxCC.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCC.SelectedIndex = 0;

            comboBoxCaroserie.Items.Clear();
            comboBoxCaroserie.Items.Add("Nespecificat");
            comboBoxCaroserie.Items.Add("Sedan");
            comboBoxCaroserie.Items.Add("Combi");
            comboBoxCaroserie.Items.Add("Cabriolet");
            comboBoxCaroserie.Items.Add("Coupe");
            comboBoxCaroserie.Items.Add("Pick-up");
            comboBoxCaroserie.Items.Add("Off-road");
            comboBoxCaroserie.Items.Add("Alta caroserie");
            comboBoxCaroserie.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCaroserie.SelectedIndex = 0;

            comboBoxCuloare.Items.Clear();
            comboBoxCuloare.Items.Add("Nespecificat");
            comboBoxCuloare.Items.Add("Albastru");
            comboBoxCuloare.Items.Add("Rosu");
            comboBoxCuloare.Items.Add("Galben");
            comboBoxCuloare.Items.Add("Verde");
            comboBoxCuloare.Items.Add("Gri");
            comboBoxCuloare.Items.Add("Negru");
            comboBoxCuloare.Items.Add("Alb");
            comboBoxCuloare.Items.Add("Maro");
            comboBoxCuloare.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCuloare.SelectedIndex = 0;

            comboBoxCutVit.Items.Clear();
            comboBoxCutVit.Items.Add("Nespecificat");
            comboBoxCutVit.Items.Add("Manuala");
            comboBoxCutVit.Items.Add("Automata");
            comboBoxCutVit.Items.Add("Semi-automata");
            comboBoxCutVit.Items.Add("Secventiala");
            comboBoxCutVit.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCutVit.SelectedIndex = 0;

            comboBoxCombustibil.Items.Clear();
            comboBoxCombustibil.Items.Add("Nespecificat");
            comboBoxCombustibil.Items.Add("Benzina FTW");
            comboBoxCombustibil.Items.Add("Motorina");
            comboBoxCombustibil.Items.Add("GPL");
            comboBoxCombustibil.Items.Add("Hybrid");
            comboBoxCombustibil.Items.Add("Electric");
            comboBoxCombustibil.Items.Add("Altul");
            comboBoxCombustibil.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxCombustibil.SelectedIndex = 0;
        }

        public System.Drawing.Image ConvertByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray);
            System.Drawing.Image rez = System.Drawing.Image.FromStream(ms);
            return rez;
        }

        private void filterHandler()
        {
            string marca = textBoxMarca.Text;
            string model = textBoxModel.Text;
            string pretmin = textBoxPretmin.Text;
            string pretmax = textBoxPretmax.Text;
            string varianta = textBoxVarianta.Text;
            string combustibil = comboBoxCombustibil.SelectedItem.ToString();
            string anFabricatie = comboBoxAn.SelectedItem.ToString();
            string cc = comboBoxCC.SelectedItem.ToString();
            string puteremin = textBoxPuteremin.Text;
            string puteremax = textBoxPuteremax.Text;
            string kmmin = textBoxKmmin.Text;
            string kmmax = textBoxKmmax.Text;
            string caroserie = comboBoxCaroserie.SelectedItem.ToString();
            string culoare = comboBoxCuloare.SelectedItem.ToString();
            string cutviteze = comboBoxCutVit.SelectedItem.ToString();
            string locatie = textBoxLocatie.Text;

            string anmin = "0", anmax = "0";
            string ccmin = "0", ccmax = "0";

            if (marca.Equals("Nespecificat")) marca = "Neselectat";
            if (model.Equals("Nespecificat")) model = "Neselectat";
            if (varianta.Equals("ex:GTI,RS,OPC,ST")) varianta = "Neselectat";
            if (pretmin.Equals("De la")) pretmin = "0";
            if (pretmax.Equals("Pana la")) pretmax = "0";
            if (combustibil.Equals("Nespecificat")) combustibil = "Neselectat";
            if (anFabricatie.Equals("Nespecificat"))
            {
                anmin = "0";
                anmax = "0";
            }
            else
            {
                if (anFabricatie.Equals("< 1970"))
                {
                    anmin = "0";
                    anmax = "1970";
                }
                if (anFabricatie.Equals("1971 - 1975"))
                {
                    anmin = "1971";
                    anmax = "1975";
                }
                if (anFabricatie.Equals("1976 - 1980"))
                {
                    anmin = "1976";
                    anmax = "1980";
                }
                if (anFabricatie.Equals("1981 - 1985"))
                {
                    anmin = "1981";
                    anmax = "1985";
                }
                if (anFabricatie.Equals("1986 - 1990"))
                {
                    anmin = "1986";
                    anmax = "1990";
                }
                if (anFabricatie.Equals("1991 - 1995"))
                {
                    anmin = "1991";
                    anmax = "1995";
                }
                if (anFabricatie.Equals("1996 - 2000"))
                {
                    anmin = "1996";
                    anmax = "2000";
                }
                if (anFabricatie.Equals("2001 - 2005"))
                {
                    anmin = "2001";
                    anmax = "2005";
                }
                if (anFabricatie.Equals("2006 - 2010"))
                {
                    anmin = "2006";
                    anmax = "2010";
                }
                if (anFabricatie.Equals("2011 - 2015"))
                {
                    anmin = "2011";
                    anmax = "2015";
                }
                if (anFabricatie.Equals("2016 - 2020"))
                {
                    anmin = "2016";
                    anmax = "2020";
                }
            }
            if (cc.Equals("Nespecificat"))
            {
                ccmin = "0";
                ccmax = "0";
            }
            else
            {
                if (cc.Equals("< 50 cc"))
                {
                    ccmin = "0";
                    ccmax = "50";
                }
                if (cc.Equals("51 - 200 cc"))
                {
                    ccmin = "51";
                    ccmax = "200";
                }
                if (cc.Equals("201 - 400 cc"))
                {
                    ccmin = "201";
                    ccmax = "400";
                }
                if (cc.Equals("401 - 600 cc"))
                {
                    ccmin = "401";
                    ccmax = "600";
                }
                if (cc.Equals("601 - 800 cc"))
                {
                    ccmin = "601";
                    ccmax = "800";
                }
                if (cc.Equals("801 - 1000 cc"))
                {
                    ccmin = "801";
                    ccmax = "1000";
                }
                if (cc.Equals("1001 - 1400 cc"))
                {
                    ccmin = "1001";
                    ccmax = "1400";
                }
                if (cc.Equals("1401 - 1800 cc"))
                {
                    ccmin = "1401";
                    ccmax = "1800";
                }
                if (cc.Equals("1801 - 2200 cc"))
                {
                    ccmin = "1801";
                    ccmax = "2200";
                }
                if (cc.Equals("2201 - 3000 cc"))
                {
                    ccmin = "2201";
                    ccmax = "3000";
                }
                if (cc.Equals("> 3001 cc"))
                {
                    ccmin = "3001";
                    ccmax = "30000";
                }
            }
            if (puteremin.Equals("De la")) puteremin = "0";
            if (puteremax.Equals("Pana la")) puteremax = "0";
            if (kmmin.Equals("De la")) kmmin = "0";
            if (kmmax.Equals("Pana la")) kmmax = "0";
            if (caroserie.Equals("Nespecificat")) caroserie = "Neselectat";
            if (cutviteze.Equals("Nespecificat")) cutviteze = "Neselectat";
            if (locatie.Equals("Nespecificat")) locatie = "Neselectat";
            if (culoare.Equals("Nespecificat")) culoare = "Neselectat";

            Console.WriteLine(marca + model + pretmin + pretmax + varianta + combustibil + anmin + anmax + ccmin + ccmax + puteremin + puteremax + kmmin + kmmax + caroserie + culoare + cutviteze + locatie);
            dsAnunturi = CatalogueService.PopulateAnunturi(marca, model, pretmin, pretmax, varianta, combustibil, anmin, anmax, ccmin, ccmax, puteremin, puteremax, kmmin, kmmax, caroserie, culoare, cutviteze, locatie, "");
            this.dataGridView1.DataSource = dsAnunturi.Tables["Anunturi"].DefaultView;
        }

        private void buttonFiltre_Click(object sender, EventArgs e)
        {
            filterHandler();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            resetFilters();
        }

        private void buttonAnunturileMele_Click(object sender, EventArgs e)
        {
            MyAdsForm mAF = new MyAdsForm(loggedInUser);
            mAF.Show();
        }

        private void butonAdaugaAnunt_Click(object sender, EventArgs e)
        {
            AddAnuntForm aAf = new AddAnuntForm(loggedInUser);
            aAf.Show();
        }

        private void buttonVeziAnunt_Click(object sender, EventArgs e)
        {
          if (dataGridView1.SelectedCells.Count > 0) {
                int rowIndex = dataGridView1.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[rowIndex];
                AnnounceHandler anunt = ViewAnuntService.getAnounceData(int.Parse(selectedRow.Cells["CodAnunt"].Value.ToString()));
                ViewAnuntForm view = new ViewAnuntForm(anunt, loggedInUser);
                view.Show();
            }
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedCells.Count > 0)
            {
                int rowIndex = dataGridView1.CurrentRow.Index;
                Console.WriteLine(rowIndex);
                DataGridViewRow selectedRow = dataGridView1.Rows[rowIndex];
                byte[] imanunt = (byte[]) selectedRow.Cells[20].Value;
                pictureBox2.Image = ConvertByteArrayToImage(imanunt);
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void buttonLogOut_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAdminTools_Click(object sender, EventArgs e)
        {
            AdminTools adminT = new AdminTools();
            adminT.Show();
        }
    }
}
