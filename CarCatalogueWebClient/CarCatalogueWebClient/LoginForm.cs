﻿using CarCatalogueWebClient.LoginServiceReference;
using System;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{
    
    public partial class LoginForm : Form
    {
        string enteredEmail = "";
        string enteredPassw = "";
        bool isAdmin = false;
        UserHandler user;
        AdminHandler admin;

        CarCatalogueWebClient.LoginServiceReference.LoginServiceSoapClient LoginService 
            = new CarCatalogueWebClient.LoginServiceReference.LoginServiceSoapClient();
        
        public LoginForm()
        {
            InitializeComponent();
            PassTextBox.PasswordChar = '*';
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            enteredEmail = EmailTextBox.Text;
            enteredPassw = PassTextBox.Text;


            if (IsValidEmail(enteredEmail))
            {
                //adresa de email valida
                if (LoginService.EmailIsFoundInDataBase(enteredEmail, isAdmin))
                {
                    //emailul este in database
                    if (LoginService.CheckPassword(enteredEmail, enteredPassw, isAdmin))
                    {
                        //login success new catalogue form
                        if (!isAdmin)
                        { 
                            Console.WriteLine("Logat cu success: " + enteredEmail);
                            user = LoginService.ReturnUserFromDataBase(enteredEmail);
                            if (user.isBanned == 0)
                            {
                                CarCatalogueForm cf = new CarCatalogueForm(user);
                                cf.Show();
                            }
                            else
                            {
                                new ErrorForm(17);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Logat cu success (admin): " + enteredEmail);
                            admin = LoginService.ReturnAdminFromDataBase(enteredEmail);
                            CarCatalogueForm cf = new CarCatalogueForm(admin);
                            cf.Show();
                        }
                    }
                    else
                    {
                        //parola incorecta
                        new ErrorForm(1);
                        Console.WriteLine("Parola incorecta pt: " + enteredEmail);
                    }
                }
                else
                {
                    //eroare emailul nu este in database
                    new ErrorForm(2);
                    Console.WriteLine("Emailul: " + enteredEmail + " nu exista in database,");
                }
            }
            else
            {
                //eroare invalid email
                new ErrorForm(3);
                Console.WriteLine(enteredEmail + " nu este un email valid.");
            }

        }

        private bool IsValidEmail(string email)
        {
            //verifica daca adresa de email de input este in format de email "ceva@provider.net"
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void IsAdminCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            //seteaza tipul de login pentru user normal/administrator
            if (IsAdminCheckBox.Checked) this.isAdmin = true;
            else this.isAdmin = false;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CreateAccountForm createAccount = new CreateAccountForm(this);
            createAccount.Show();
        }
    }
}
