﻿using CarCatalogueWebClient.LoginServiceReference;
using CarCatalogueWebClient.ViewAnnounceService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{
    public partial class MyAdsForm : Form
    {
        UserHandler loggedInUser;
        DataSet dsAnunturi;

        CarCatalogueWebClient.CarCatalogueServiceReference.CarCatalogueServiceSoapClient ServiceAnunturileMele
         = new CarCatalogueWebClient.CarCatalogueServiceReference.CarCatalogueServiceSoapClient();


        CarCatalogueWebClient.ViewAnnounceService.ViewAnuntServiceSoapClient ViewAnuntService
            = new CarCatalogueWebClient.ViewAnnounceService.ViewAnuntServiceSoapClient();

        public MyAdsForm(UserHandler user)
        {
            InitializeComponent();
            this.loggedInUser = user;
            this.Text = "Anunturile utilizatorului:"+ user.nume + " " + user.prenume;
            dsAnunturi = ServiceAnunturileMele.MyAnunturi(user.id.ToString());
            this.dataGridView1.DataSource = dsAnunturi.Tables["Anunturi"].DefaultView;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            AddAnuntForm aAf = new AddAnuntForm(loggedInUser);
            aAf.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridView1.Rows[index];
            int Codanunt = int.Parse(selectedRow.Cells[1].Value.ToString());
            Console.WriteLine(Codanunt);
            AnnounceHandler anunt = ViewAnuntService.getAnounceData(Codanunt); 
            AddAnuntForm aAf = new AddAnuntForm(loggedInUser, anunt);
            aAf.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridView1.Rows[index];
            int Codanunt = int.Parse(selectedRow.Cells[1].Value.ToString());
            Console.WriteLine(Codanunt);
            ViewAnuntService.deleteAnunt(Codanunt);
        }
    }
}
