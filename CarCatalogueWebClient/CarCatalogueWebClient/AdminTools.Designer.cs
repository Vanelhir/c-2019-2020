﻿namespace CarCatalogueWebClient
{
    partial class AdminTools
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonCautaUser = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.buttonBan = new System.Windows.Forms.Button();
            this.buttonUnBan = new System.Windows.Forms.Button();
            this.buttonUnReport = new System.Windows.Forms.Button();
            this.buttonStergeUser = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 79);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1276, 529);
            this.dataGridView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Useri:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 29);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1195, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "Cauta user dupa Id";
            // 
            // buttonCautaUser
            // 
            this.buttonCautaUser.Location = new System.Drawing.Point(1213, 27);
            this.buttonCautaUser.Name = "buttonCautaUser";
            this.buttonCautaUser.Size = new System.Drawing.Size(75, 23);
            this.buttonCautaUser.TabIndex = 6;
            this.buttonCautaUser.Text = "Cauta";
            this.buttonCautaUser.UseVisualStyleBackColor = true;
            this.buttonCautaUser.Click += new System.EventHandler(this.buttonCautaUser_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(1166, 614);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(122, 17);
            this.checkBox1.TabIndex = 8;
            this.checkBox1.Text = "Doar Useri Raportati";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // buttonBan
            // 
            this.buttonBan.Location = new System.Drawing.Point(1044, 662);
            this.buttonBan.Name = "buttonBan";
            this.buttonBan.Size = new System.Drawing.Size(75, 23);
            this.buttonBan.TabIndex = 10;
            this.buttonBan.Text = "Ban";
            this.buttonBan.UseVisualStyleBackColor = true;
            this.buttonBan.Click += new System.EventHandler(this.buttonBan_Click);
            // 
            // buttonUnBan
            // 
            this.buttonUnBan.Location = new System.Drawing.Point(1125, 662);
            this.buttonUnBan.Name = "buttonUnBan";
            this.buttonUnBan.Size = new System.Drawing.Size(75, 23);
            this.buttonUnBan.TabIndex = 11;
            this.buttonUnBan.Text = "Unban";
            this.buttonUnBan.UseVisualStyleBackColor = true;
            this.buttonUnBan.Click += new System.EventHandler(this.buttonUnBan_Click);
            // 
            // buttonUnReport
            // 
            this.buttonUnReport.Location = new System.Drawing.Point(1206, 662);
            this.buttonUnReport.Name = "buttonUnReport";
            this.buttonUnReport.Size = new System.Drawing.Size(75, 23);
            this.buttonUnReport.TabIndex = 12;
            this.buttonUnReport.Text = "Unreport";
            this.buttonUnReport.UseVisualStyleBackColor = true;
            this.buttonUnReport.Click += new System.EventHandler(this.buttonUnReport_Click);
            // 
            // buttonStergeUser
            // 
            this.buttonStergeUser.Location = new System.Drawing.Point(963, 662);
            this.buttonStergeUser.Name = "buttonStergeUser";
            this.buttonStergeUser.Size = new System.Drawing.Size(75, 23);
            this.buttonStergeUser.TabIndex = 14;
            this.buttonStergeUser.Text = "Sterge";
            this.buttonStergeUser.UseVisualStyleBackColor = true;
            this.buttonStergeUser.Click += new System.EventHandler(this.buttonStergeUser_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(1036, 614);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(109, 17);
            this.checkBox3.TabIndex = 15;
            this.checkBox3.Text = "Doar Useri Banati";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // AdminTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1314, 697);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.buttonStergeUser);
            this.Controls.Add(this.buttonUnReport);
            this.Controls.Add(this.buttonUnBan);
            this.Controls.Add(this.buttonBan);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.buttonCautaUser);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "AdminTools";
            this.Text = "AdminTools";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonCautaUser;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button buttonBan;
        private System.Windows.Forms.Button buttonUnBan;
        private System.Windows.Forms.Button buttonUnReport;
        private System.Windows.Forms.Button buttonStergeUser;
        private System.Windows.Forms.CheckBox checkBox3;
    }
}