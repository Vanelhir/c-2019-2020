﻿namespace CarCatalogueWebClient
{
    partial class CarCatalogueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxPuteremax = new System.Windows.Forms.TextBox();
            this.textBoxPuteremin = new System.Windows.Forms.TextBox();
            this.textBoxPretmax = new System.Windows.Forms.TextBox();
            this.textBoxPretmin = new System.Windows.Forms.TextBox();
            this.textBoxLocatie = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.textBoxMarca = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxKmmin = new System.Windows.Forms.TextBox();
            this.textBoxKmmax = new System.Windows.Forms.TextBox();
            this.buttonFiltre = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxCuloare = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBoxCombustibil = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxVarianta = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxCutVit = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxCaroserie = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxCC = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxAn = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelMarcă = new System.Windows.Forms.Label();
            this.labelCC = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.LabelNumeUtilizator = new System.Windows.Forms.Label();
            this.buttonLogOut = new System.Windows.Forms.Button();
            this.buttonAnunturileMele = new System.Windows.Forms.Button();
            this.butonAdaugaAnunt = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonVeziAnunt = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.buttonAdminTools = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxPuteremax);
            this.groupBox1.Controls.Add(this.textBoxPuteremin);
            this.groupBox1.Controls.Add(this.textBoxPretmax);
            this.groupBox1.Controls.Add(this.textBoxPretmin);
            this.groupBox1.Controls.Add(this.textBoxLocatie);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBoxModel);
            this.groupBox1.Controls.Add(this.textBoxMarca);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.textBoxKmmin);
            this.groupBox1.Controls.Add(this.textBoxKmmax);
            this.groupBox1.Controls.Add(this.buttonFiltre);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.comboBoxCuloare);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.comboBoxCombustibil);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.textBoxVarianta);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.comboBoxCutVit);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.comboBoxCaroserie);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.comboBoxCC);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxAn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labelMarcă);
            this.groupBox1.Controls.Add(this.labelCC);
            this.groupBox1.Location = new System.Drawing.Point(10, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1450, 159);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtre autovehicul";
            // 
            // textBoxPuteremax
            // 
            this.textBoxPuteremax.Location = new System.Drawing.Point(803, 40);
            this.textBoxPuteremax.Name = "textBoxPuteremax";
            this.textBoxPuteremax.Size = new System.Drawing.Size(111, 20);
            this.textBoxPuteremax.TabIndex = 52;
            this.textBoxPuteremax.Text = "Pana la";
            // 
            // textBoxPuteremin
            // 
            this.textBoxPuteremin.Location = new System.Drawing.Point(670, 41);
            this.textBoxPuteremin.Name = "textBoxPuteremin";
            this.textBoxPuteremin.Size = new System.Drawing.Size(111, 20);
            this.textBoxPuteremin.TabIndex = 51;
            this.textBoxPuteremin.Text = "De la";
            // 
            // textBoxPretmax
            // 
            this.textBoxPretmax.Location = new System.Drawing.Point(317, 40);
            this.textBoxPretmax.Name = "textBoxPretmax";
            this.textBoxPretmax.Size = new System.Drawing.Size(100, 20);
            this.textBoxPretmax.TabIndex = 50;
            this.textBoxPretmax.Text = "Pana la";
            // 
            // textBoxPretmin
            // 
            this.textBoxPretmin.Location = new System.Drawing.Point(184, 40);
            this.textBoxPretmin.Name = "textBoxPretmin";
            this.textBoxPretmin.Size = new System.Drawing.Size(111, 20);
            this.textBoxPretmin.TabIndex = 49;
            this.textBoxPretmin.Text = "De la";
            // 
            // textBoxLocatie
            // 
            this.textBoxLocatie.Location = new System.Drawing.Point(1075, 88);
            this.textBoxLocatie.Name = "textBoxLocatie";
            this.textBoxLocatie.Size = new System.Drawing.Size(114, 20);
            this.textBoxLocatie.TabIndex = 48;
            this.textBoxLocatie.Text = "Nespecificat";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1195, 77);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 40);
            this.button1.TabIndex = 47;
            this.button1.Text = "Reseteaza Filtrele!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(29, 85);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(129, 20);
            this.textBoxModel.TabIndex = 46;
            this.textBoxModel.Text = "Nespecificat";
            // 
            // textBoxMarca
            // 
            this.textBoxMarca.Location = new System.Drawing.Point(29, 41);
            this.textBoxMarca.Name = "textBoxMarca";
            this.textBoxMarca.Size = new System.Drawing.Size(129, 20);
            this.textBoxMarca.TabIndex = 45;
            this.textBoxMarca.Text = "Nespecificat";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(1308, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(136, 140);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxKmmin
            // 
            this.textBoxKmmin.Location = new System.Drawing.Point(670, 87);
            this.textBoxKmmin.Name = "textBoxKmmin";
            this.textBoxKmmin.Size = new System.Drawing.Size(111, 20);
            this.textBoxKmmin.TabIndex = 41;
            this.textBoxKmmin.Text = "De la";
            // 
            // textBoxKmmax
            // 
            this.textBoxKmmax.Location = new System.Drawing.Point(803, 87);
            this.textBoxKmmax.Name = "textBoxKmmax";
            this.textBoxKmmax.Size = new System.Drawing.Size(111, 20);
            this.textBoxKmmax.TabIndex = 40;
            this.textBoxKmmax.Text = "Pana la";
            // 
            // buttonFiltre
            // 
            this.buttonFiltre.Location = new System.Drawing.Point(1195, 28);
            this.buttonFiltre.Margin = new System.Windows.Forms.Padding(2);
            this.buttonFiltre.Name = "buttonFiltre";
            this.buttonFiltre.Size = new System.Drawing.Size(97, 40);
            this.buttonFiltre.TabIndex = 39;
            this.buttonFiltre.Text = "Aplică Filtrele!";
            this.buttonFiltre.UseVisualStyleBackColor = true;
            this.buttonFiltre.Click += new System.EventHandler(this.buttonFiltre_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(1072, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Locatie:";
            // 
            // comboBoxCuloare
            // 
            this.comboBoxCuloare.Location = new System.Drawing.Point(1075, 39);
            this.comboBoxCuloare.Name = "comboBoxCuloare";
            this.comboBoxCuloare.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxCuloare.Size = new System.Drawing.Size(114, 21);
            this.comboBoxCuloare.TabIndex = 36;
            this.comboBoxCuloare.Text = "Neselectat";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1072, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Culoare:";
            // 
            // comboBoxCombustibil
            // 
            this.comboBoxCombustibil.Location = new System.Drawing.Point(317, 84);
            this.comboBoxCombustibil.Name = "comboBoxCombustibil";
            this.comboBoxCombustibil.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxCombustibil.Size = new System.Drawing.Size(100, 21);
            this.comboBoxCombustibil.TabIndex = 34;
            this.comboBoxCombustibil.Text = "Neselectat";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(314, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Combustibil:";
            // 
            // textBoxVarianta
            // 
            this.textBoxVarianta.Location = new System.Drawing.Point(184, 85);
            this.textBoxVarianta.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxVarianta.Name = "textBoxVarianta";
            this.textBoxVarianta.Size = new System.Drawing.Size(111, 20);
            this.textBoxVarianta.TabIndex = 32;
            this.textBoxVarianta.Text = "ex:GTI,RS,OPC,ST";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(181, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Variantă:";
            // 
            // comboBoxCutVit
            // 
            this.comboBoxCutVit.Location = new System.Drawing.Point(922, 87);
            this.comboBoxCutVit.Name = "comboBoxCutVit";
            this.comboBoxCutVit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxCutVit.Size = new System.Drawing.Size(134, 21);
            this.comboBoxCutVit.TabIndex = 30;
            this.comboBoxCutVit.Text = "Neselectat";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(919, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Cutie de viteze:";
            // 
            // comboBoxCaroserie
            // 
            this.comboBoxCaroserie.Location = new System.Drawing.Point(922, 39);
            this.comboBoxCaroserie.Name = "comboBoxCaroserie";
            this.comboBoxCaroserie.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxCaroserie.Size = new System.Drawing.Size(134, 21);
            this.comboBoxCaroserie.TabIndex = 28;
            this.comboBoxCaroserie.Text = "Neselectat";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(919, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Caroserie:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(787, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(667, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Putere:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(787, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "-";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(667, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Kilometraj:";
            // 
            // comboBoxCC
            // 
            this.comboBoxCC.Location = new System.Drawing.Point(541, 84);
            this.comboBoxCC.Name = "comboBoxCC";
            this.comboBoxCC.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxCC.Size = new System.Drawing.Size(102, 21);
            this.comboBoxCC.TabIndex = 16;
            this.comboBoxCC.Text = "Nespecificat";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(301, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(181, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Pret:";
            // 
            // comboBoxAn
            // 
            this.comboBoxAn.Location = new System.Drawing.Point(541, 41);
            this.comboBoxAn.Name = "comboBoxAn";
            this.comboBoxAn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxAn.Size = new System.Drawing.Size(102, 21);
            this.comboBoxAn.TabIndex = 9;
            this.comboBoxAn.Text = "Nespecificat";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(451, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "An de fabricație:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Model:";
            // 
            // labelMarcă
            // 
            this.labelMarcă.AutoSize = true;
            this.labelMarcă.Location = new System.Drawing.Point(26, 24);
            this.labelMarcă.Name = "labelMarcă";
            this.labelMarcă.Size = new System.Drawing.Size(40, 13);
            this.labelMarcă.TabIndex = 4;
            this.labelMarcă.Text = "Marcă:";
            // 
            // labelCC
            // 
            this.labelCC.AutoSize = true;
            this.labelCC.Location = new System.Drawing.Point(430, 87);
            this.labelCC.Name = "labelCC";
            this.labelCC.Size = new System.Drawing.Size(105, 13);
            this.labelCC.TabIndex = 2;
            this.labelCC.Text = "Capacitate cilindrică:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 175);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Rezultate";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(1338, 191);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Inregistrat ca:";
            // 
            // LabelNumeUtilizator
            // 
            this.LabelNumeUtilizator.AutoSize = true;
            this.LabelNumeUtilizator.Location = new System.Drawing.Point(1315, 213);
            this.LabelNumeUtilizator.Name = "LabelNumeUtilizator";
            this.LabelNumeUtilizator.Size = new System.Drawing.Size(123, 13);
            this.LabelNumeUtilizator.TabIndex = 4;
            this.LabelNumeUtilizator.Text = "Nume Prenume Utilizator";
            // 
            // buttonLogOut
            // 
            this.buttonLogOut.Location = new System.Drawing.Point(1318, 240);
            this.buttonLogOut.Name = "buttonLogOut";
            this.buttonLogOut.Size = new System.Drawing.Size(120, 23);
            this.buttonLogOut.TabIndex = 5;
            this.buttonLogOut.Text = "Log Out";
            this.buttonLogOut.UseVisualStyleBackColor = true;
            this.buttonLogOut.Click += new System.EventHandler(this.buttonLogOut_Click);
            // 
            // buttonAnunturileMele
            // 
            this.buttonAnunturileMele.Location = new System.Drawing.Point(1318, 269);
            this.buttonAnunturileMele.Name = "buttonAnunturileMele";
            this.buttonAnunturileMele.Size = new System.Drawing.Size(120, 23);
            this.buttonAnunturileMele.TabIndex = 6;
            this.buttonAnunturileMele.Text = "Anunturile Mele";
            this.buttonAnunturileMele.UseVisualStyleBackColor = true;
            this.buttonAnunturileMele.Click += new System.EventHandler(this.buttonAnunturileMele_Click);
            // 
            // butonAdaugaAnunt
            // 
            this.butonAdaugaAnunt.Location = new System.Drawing.Point(1318, 298);
            this.butonAdaugaAnunt.Name = "butonAdaugaAnunt";
            this.butonAdaugaAnunt.Size = new System.Drawing.Size(120, 23);
            this.butonAdaugaAnunt.TabIndex = 7;
            this.butonAdaugaAnunt.Text = "Adauga Anunt";
            this.butonAdaugaAnunt.UseVisualStyleBackColor = true;
            this.butonAdaugaAnunt.Click += new System.EventHandler(this.butonAdaugaAnunt_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 191);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1297, 514);
            this.dataGridView1.TabIndex = 8;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick_1);
            // 
            // buttonVeziAnunt
            // 
            this.buttonVeziAnunt.Location = new System.Drawing.Point(1318, 327);
            this.buttonVeziAnunt.Name = "buttonVeziAnunt";
            this.buttonVeziAnunt.Size = new System.Drawing.Size(120, 23);
            this.buttonVeziAnunt.TabIndex = 9;
            this.buttonVeziAnunt.Text = "Vezi anunt";
            this.buttonVeziAnunt.UseVisualStyleBackColor = true;
            this.buttonVeziAnunt.Click += new System.EventHandler(this.buttonVeziAnunt_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(1324, 367);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(136, 140);
            this.pictureBox2.TabIndex = 53;
            this.pictureBox2.TabStop = false;
            // 
            // buttonAdminTools
            // 
            this.buttonAdminTools.Location = new System.Drawing.Point(1324, 682);
            this.buttonAdminTools.Name = "buttonAdminTools";
            this.buttonAdminTools.Size = new System.Drawing.Size(120, 23);
            this.buttonAdminTools.TabIndex = 54;
            this.buttonAdminTools.Text = "Admin Tools";
            this.buttonAdminTools.UseVisualStyleBackColor = true;
            this.buttonAdminTools.Click += new System.EventHandler(this.buttonAdminTools_Click);
            // 
            // CarCatalogueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1472, 717);
            this.Controls.Add(this.buttonAdminTools);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.buttonVeziAnunt);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.butonAdaugaAnunt);
            this.Controls.Add(this.buttonAnunturileMele);
            this.Controls.Add(this.buttonLogOut);
            this.Controls.Add(this.LabelNumeUtilizator);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.groupBox1);
            this.Name = "CarCatalogueForm";
            this.Text = "CarCatalogueForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelCC;
        private System.Windows.Forms.Label labelMarcă;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxAn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBoxCutVit;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxCaroserie;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxCC;
        private System.Windows.Forms.Button buttonFiltre;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBoxCuloare;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBoxCombustibil;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxKmmin;
        private System.Windows.Forms.TextBox textBoxKmmax;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label LabelNumeUtilizator;
        private System.Windows.Forms.Button buttonLogOut;
        private System.Windows.Forms.Button buttonAnunturileMele;
        private System.Windows.Forms.Button butonAdaugaAnunt;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.TextBox textBoxMarca;
        private System.Windows.Forms.TextBox textBoxVarianta;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxLocatie;
        private System.Windows.Forms.TextBox textBoxPretmax;
        private System.Windows.Forms.TextBox textBoxPretmin;
        private System.Windows.Forms.TextBox textBoxPuteremax;
        private System.Windows.Forms.TextBox textBoxPuteremin;
        private System.Windows.Forms.Button buttonVeziAnunt;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button buttonAdminTools;
    }
}