﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{
    public partial class TestForm : Form
    {
        byte[] imageArray;
        public TestForm(byte[] image)
        {
            InitializeComponent();
            this.imageArray = image;
            Image cacat = ConvertByteArrayToImage(this.imageArray);
            pictureBox1.Image = cacat;
        }

        private Image ConvertByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray);
            Image rez = Image.FromStream(ms);
            return rez;
        }
    }
}
