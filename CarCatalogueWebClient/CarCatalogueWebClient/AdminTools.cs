﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarCatalogueWebClient
{
    public partial class AdminTools : Form
    {

        CarCatalogueWebClient.AdminToolServiceReference.AdminToolServiceSoapClient AdminToolService
            = new CarCatalogueWebClient.AdminToolServiceReference.AdminToolServiceSoapClient();

        DataSet dsUseri;

        public AdminTools()
        {
            InitializeComponent();
            dsUseri = AdminToolService.PopulateUsers(false, false);
            dataGridView1.DataSource = dsUseri.Tables["Useri"].DefaultView;
        }

        private void buttonCautaUser_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox1.Text, out int idUser))
            {
                dsUseri = AdminToolService.SearchUsers(checkBox1.Checked, checkBox3.Checked, idUser);
                dataGridView1.DataSource = dsUseri.Tables["Useri"].DefaultView;
            }
            else
            {
                //eroare nu este int
                dsUseri = AdminToolService.PopulateUsers(checkBox1.Checked, checkBox3.Checked);
                dataGridView1.DataSource = dsUseri.Tables["Useri"].DefaultView;
            }
        }

        private void buttonBan_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridView1.Rows[index];
            int id = int.Parse(selectedRow.Cells[0].Value.ToString());
            Console.WriteLine(id+" "+index);
            AdminToolService.banUser(id);
        }

        private void buttonUnBan_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridView1.Rows[index];
            int id = int.Parse(selectedRow.Cells[0].Value.ToString());
            AdminToolService.unbanUser(id);
        }

        private void buttonUnReport_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridView1.Rows[index];
            int id = int.Parse(selectedRow.Cells[0].Value.ToString());
            AdminToolService.unreportUser(id);
        }

        private void buttonStergeUser_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow selectedRow = dataGridView1.Rows[index];
            int id = int.Parse(selectedRow.Cells[0].Value.ToString());
            AdminToolService.deleteUser(id);
        }
    }
}
