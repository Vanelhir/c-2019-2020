﻿namespace CarCatalogueWebClient
{
    partial class ViewAnuntForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewAnuntForm));
            this.label16 = new System.Windows.Forms.Label();
            this.labelLocatie = new System.Windows.Forms.Label();
            this.labelCuloare = new System.Windows.Forms.Label();
            this.labelCutie = new System.Windows.Forms.Label();
            this.labelCC = new System.Windows.Forms.Label();
            this.labelCombustibil = new System.Windows.Forms.Label();
            this.labelPretLicitatieCurenta = new System.Windows.Forms.Label();
            this.labelKW = new System.Windows.Forms.Label();
            this.labelCP = new System.Windows.Forms.Label();
            this.labelkm = new System.Windows.Forms.Label();
            this.labelAn = new System.Windows.Forms.Label();
            this.labelPretVanzare = new System.Windows.Forms.Label();
            this.labelCaroserie = new System.Windows.Forms.Label();
            this.labelVarianta = new System.Windows.Forms.Label();
            this.buttonInchidere = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxLicitatie = new System.Windows.Forms.TextBox();
            this.labelCod = new System.Windows.Forms.Label();
            this.labelTelefonVanzator = new System.Windows.Forms.Label();
            this.labelNumeVanzator = new System.Windows.Forms.Label();
            this.pictureBoxVanzator = new System.Windows.Forms.PictureBox();
            this.buttonReport = new System.Windows.Forms.Button();
            this.buttonLiciteaza = new System.Windows.Forms.Button();
            this.richTextBoxDescriere = new System.Windows.Forms.RichTextBox();
            this.labelModel = new System.Windows.Forms.Label();
            this.labelMarca = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button6 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVanzator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(61, 102);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 20);
            this.label16.TabIndex = 40;
            this.label16.Text = "Desciere";
            // 
            // labelLocatie
            // 
            this.labelLocatie.AutoSize = true;
            this.labelLocatie.Location = new System.Drawing.Point(1213, 263);
            this.labelLocatie.Name = "labelLocatie";
            this.labelLocatie.Size = new System.Drawing.Size(65, 20);
            this.labelLocatie.TabIndex = 39;
            this.labelLocatie.Text = "Locatie:";
            // 
            // labelCuloare
            // 
            this.labelCuloare.AutoSize = true;
            this.labelCuloare.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelCuloare.Location = new System.Drawing.Point(704, 78);
            this.labelCuloare.Name = "labelCuloare";
            this.labelCuloare.Size = new System.Drawing.Size(61, 17);
            this.labelCuloare.TabIndex = 38;
            this.labelCuloare.Text = "Culoare:";
            // 
            // labelCutie
            // 
            this.labelCutie.AutoSize = true;
            this.labelCutie.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelCutie.Location = new System.Drawing.Point(63, 74);
            this.labelCutie.Name = "labelCutie";
            this.labelCutie.Size = new System.Drawing.Size(105, 17);
            this.labelCutie.TabIndex = 37;
            this.labelCutie.Text = "Cutie de viteze:";
            // 
            // labelCC
            // 
            this.labelCC.AutoSize = true;
            this.labelCC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelCC.Location = new System.Drawing.Point(879, 41);
            this.labelCC.Name = "labelCC";
            this.labelCC.Size = new System.Drawing.Size(30, 17);
            this.labelCC.TabIndex = 37;
            this.labelCC.Text = "CC:";
            // 
            // labelCombustibil
            // 
            this.labelCombustibil.AutoSize = true;
            this.labelCombustibil.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelCombustibil.Location = new System.Drawing.Point(546, 78);
            this.labelCombustibil.Name = "labelCombustibil";
            this.labelCombustibil.Size = new System.Drawing.Size(84, 17);
            this.labelCombustibil.TabIndex = 36;
            this.labelCombustibil.Text = "Combustibil:";
            // 
            // labelPretLicitatieCurenta
            // 
            this.labelPretLicitatieCurenta.AutoSize = true;
            this.labelPretLicitatieCurenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelPretLicitatieCurenta.Location = new System.Drawing.Point(966, 128);
            this.labelPretLicitatieCurenta.Name = "labelPretLicitatieCurenta";
            this.labelPretLicitatieCurenta.Size = new System.Drawing.Size(143, 20);
            this.labelPretLicitatieCurenta.TabIndex = 35;
            this.labelPretLicitatieCurenta.Text = "Pret licitatie curent:";
            // 
            // labelKW
            // 
            this.labelKW.AutoSize = true;
            this.labelKW.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelKW.Location = new System.Drawing.Point(388, 74);
            this.labelKW.Name = "labelKW";
            this.labelKW.Size = new System.Drawing.Size(80, 17);
            this.labelKW.TabIndex = 34;
            this.labelKW.Text = "Putere KW:";
            // 
            // labelCP
            // 
            this.labelCP.AutoSize = true;
            this.labelCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelCP.Location = new System.Drawing.Point(258, 74);
            this.labelCP.Name = "labelCP";
            this.labelCP.Size = new System.Drawing.Size(76, 17);
            this.labelCP.TabIndex = 33;
            this.labelCP.Text = "Putere CP:";
            // 
            // labelkm
            // 
            this.labelkm.AutoSize = true;
            this.labelkm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelkm.Location = new System.Drawing.Point(704, 37);
            this.labelkm.Name = "labelkm";
            this.labelkm.Size = new System.Drawing.Size(74, 17);
            this.labelkm.TabIndex = 32;
            this.labelkm.Text = "Kilometraj:";
            // 
            // labelAn
            // 
            this.labelAn.AutoSize = true;
            this.labelAn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelAn.Location = new System.Drawing.Point(558, 37);
            this.labelAn.Name = "labelAn";
            this.labelAn.Size = new System.Drawing.Size(91, 17);
            this.labelAn.TabIndex = 31;
            this.labelAn.Text = "An fabricatie:";
            // 
            // labelPretVanzare
            // 
            this.labelPretVanzare.AutoSize = true;
            this.labelPretVanzare.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.labelPretVanzare.Location = new System.Drawing.Point(966, 165);
            this.labelPretVanzare.Name = "labelPretVanzare";
            this.labelPretVanzare.Size = new System.Drawing.Size(146, 24);
            this.labelPretVanzare.TabIndex = 30;
            this.labelPretVanzare.Text = "Pret de vanzare:";
            // 
            // labelCaroserie
            // 
            this.labelCaroserie.AutoSize = true;
            this.labelCaroserie.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelCaroserie.Location = new System.Drawing.Point(859, 78);
            this.labelCaroserie.Name = "labelCaroserie";
            this.labelCaroserie.Size = new System.Drawing.Size(73, 17);
            this.labelCaroserie.TabIndex = 29;
            this.labelCaroserie.Text = "Caroserie:";
            // 
            // labelVarianta
            // 
            this.labelVarianta.AutoSize = true;
            this.labelVarianta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelVarianta.Location = new System.Drawing.Point(388, 37);
            this.labelVarianta.Name = "labelVarianta";
            this.labelVarianta.Size = new System.Drawing.Size(65, 17);
            this.labelVarianta.TabIndex = 28;
            this.labelVarianta.Text = "Varianta:";
            // 
            // buttonInchidere
            // 
            this.buttonInchidere.Location = new System.Drawing.Point(1477, 817);
            this.buttonInchidere.Name = "buttonInchidere";
            this.buttonInchidere.Size = new System.Drawing.Size(75, 23);
            this.buttonInchidere.TabIndex = 54;
            this.buttonInchidere.Text = "Inchide";
            this.buttonInchidere.UseVisualStyleBackColor = true;
            this.buttonInchidere.Click += new System.EventHandler(this.buttonInchidere_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxLicitatie);
            this.groupBox1.Controls.Add(this.labelCod);
            this.groupBox1.Controls.Add(this.labelTelefonVanzator);
            this.groupBox1.Controls.Add(this.labelNumeVanzator);
            this.groupBox1.Controls.Add(this.pictureBoxVanzator);
            this.groupBox1.Controls.Add(this.buttonLiciteaza);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.labelLocatie);
            this.groupBox1.Controls.Add(this.labelCuloare);
            this.groupBox1.Controls.Add(this.labelCutie);
            this.groupBox1.Controls.Add(this.labelCC);
            this.groupBox1.Controls.Add(this.labelCombustibil);
            this.groupBox1.Controls.Add(this.labelPretLicitatieCurenta);
            this.groupBox1.Controls.Add(this.labelKW);
            this.groupBox1.Controls.Add(this.labelCP);
            this.groupBox1.Controls.Add(this.labelkm);
            this.groupBox1.Controls.Add(this.richTextBoxDescriere);
            this.groupBox1.Controls.Add(this.labelAn);
            this.groupBox1.Controls.Add(this.labelPretVanzare);
            this.groupBox1.Controls.Add(this.labelCaroserie);
            this.groupBox1.Controls.Add(this.labelVarianta);
            this.groupBox1.Controls.Add(this.labelModel);
            this.groupBox1.Controls.Add(this.labelMarca);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox1.Location = new System.Drawing.Point(49, 428);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1470, 371);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalii vehicul";
            // 
            // textBoxLicitatie
            // 
            this.textBoxLicitatie.Location = new System.Drawing.Point(956, 201);
            this.textBoxLicitatie.Name = "textBoxLicitatie";
            this.textBoxLicitatie.Size = new System.Drawing.Size(100, 26);
            this.textBoxLicitatie.TabIndex = 61;
            // 
            // labelCod
            // 
            this.labelCod.AutoSize = true;
            this.labelCod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.labelCod.Location = new System.Drawing.Point(1363, 348);
            this.labelCod.Name = "labelCod";
            this.labelCod.Size = new System.Drawing.Size(59, 13);
            this.labelCod.TabIndex = 60;
            this.labelCod.Text = "Cod anunt:";
            // 
            // labelTelefonVanzator
            // 
            this.labelTelefonVanzator.AutoSize = true;
            this.labelTelefonVanzator.Location = new System.Drawing.Point(1212, 234);
            this.labelTelefonVanzator.Name = "labelTelefonVanzator";
            this.labelTelefonVanzator.Size = new System.Drawing.Size(66, 20);
            this.labelTelefonVanzator.TabIndex = 59;
            this.labelTelefonVanzator.Text = "Telefon:";
            // 
            // labelNumeVanzator
            // 
            this.labelNumeVanzator.AutoSize = true;
            this.labelNumeVanzator.Location = new System.Drawing.Point(1200, 203);
            this.labelNumeVanzator.Name = "labelNumeVanzator";
            this.labelNumeVanzator.Size = new System.Drawing.Size(78, 20);
            this.labelNumeVanzator.TabIndex = 58;
            this.labelNumeVanzator.Text = "Vanzator:";
            // 
            // pictureBoxVanzator
            // 
            this.pictureBoxVanzator.Location = new System.Drawing.Point(1287, 19);
            this.pictureBoxVanzator.Name = "pictureBoxVanzator";
            this.pictureBoxVanzator.Size = new System.Drawing.Size(167, 167);
            this.pictureBoxVanzator.TabIndex = 57;
            this.pictureBoxVanzator.TabStop = false;
            // 
            // buttonReport
            // 
            this.buttonReport.Location = new System.Drawing.Point(1396, 817);
            this.buttonReport.Name = "buttonReport";
            this.buttonReport.Size = new System.Drawing.Size(75, 23);
            this.buttonReport.TabIndex = 56;
            this.buttonReport.Text = "Report";
            this.buttonReport.UseVisualStyleBackColor = true;
            this.buttonReport.Click += new System.EventHandler(this.buttonReport_Click);
            // 
            // buttonLiciteaza
            // 
            this.buttonLiciteaza.Location = new System.Drawing.Point(1062, 199);
            this.buttonLiciteaza.Name = "buttonLiciteaza";
            this.buttonLiciteaza.Size = new System.Drawing.Size(97, 28);
            this.buttonLiciteaza.TabIndex = 55;
            this.buttonLiciteaza.Text = "Liciteaza";
            this.buttonLiciteaza.UseVisualStyleBackColor = true;
            this.buttonLiciteaza.Click += new System.EventHandler(this.buttonLiciteaza_Click);
            // 
            // richTextBoxDescriere
            // 
            this.richTextBoxDescriere.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.richTextBoxDescriere.Location = new System.Drawing.Point(66, 130);
            this.richTextBoxDescriere.Name = "richTextBoxDescriere";
            this.richTextBoxDescriere.Size = new System.Drawing.Size(852, 224);
            this.richTextBoxDescriere.TabIndex = 25;
            this.richTextBoxDescriere.Text = "";
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelModel.Location = new System.Drawing.Point(215, 37);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(50, 17);
            this.labelModel.TabIndex = 27;
            this.labelModel.Text = "Model:";
            // 
            // labelMarca
            // 
            this.labelMarca.AutoSize = true;
            this.labelMarca.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelMarca.Location = new System.Drawing.Point(63, 37);
            this.labelMarca.Name = "labelMarca";
            this.labelMarca.Size = new System.Drawing.Size(51, 17);
            this.labelMarca.TabIndex = 26;
            this.labelMarca.Text = "Marca:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(956, 330);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(136, 24);
            this.checkBox1.TabIndex = 23;
            this.checkBox1.Text = "Persoana fizica";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(1116, 330);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(68, 24);
            this.checkBox2.TabIndex = 24;
            this.checkBox2.Text = "Firma";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(1444, 776);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 55;
            this.button6.Text = "Iesi";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.InitialImage")));
            this.pictureBox4.Location = new System.Drawing.Point(1165, 78);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(354, 315);
            this.pictureBox4.TabIndex = 48;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.InitialImage")));
            this.pictureBox3.Location = new System.Drawing.Point(795, 78);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(354, 315);
            this.pictureBox3.TabIndex = 47;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
            this.pictureBox2.Location = new System.Drawing.Point(423, 78);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(354, 315);
            this.pictureBox2.TabIndex = 46;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(49, 78);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(354, 315);
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label17.Location = new System.Drawing.Point(43, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(222, 31);
            this.label17.TabIndex = 56;
            this.label17.Text = "Vizualizare anunt";
            // 
            // ViewAnuntForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1564, 852);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.buttonReport);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonInchidere);
            this.Name = "ViewAnuntForm";
            this.Text = "ViewAnuntForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVanzator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelLocatie;
        private System.Windows.Forms.Label labelCuloare;
        private System.Windows.Forms.Label labelCutie;
        private System.Windows.Forms.Label labelCC;
        private System.Windows.Forms.Label labelCombustibil;
        private System.Windows.Forms.Label labelPretLicitatieCurenta;
        private System.Windows.Forms.Label labelKW;
        private System.Windows.Forms.Label labelCP;
        private System.Windows.Forms.Label labelkm;
        private System.Windows.Forms.Label labelAn;
        private System.Windows.Forms.Label labelPretVanzare;
        private System.Windows.Forms.Label labelCaroserie;
        private System.Windows.Forms.Label labelVarianta;
        private System.Windows.Forms.Button buttonInchidere;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelCod;
        private System.Windows.Forms.Label labelTelefonVanzator;
        private System.Windows.Forms.Label labelNumeVanzator;
        private System.Windows.Forms.PictureBox pictureBoxVanzator;
        private System.Windows.Forms.Button buttonReport;
        private System.Windows.Forms.Button buttonLiciteaza;
        private System.Windows.Forms.RichTextBox richTextBoxDescriere;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.Label labelMarca;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxLicitatie;
        private System.Windows.Forms.Label label17;
    }
}